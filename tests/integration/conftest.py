from .conftest_docker import *
from .conftest_apps import *
from .conftest_seed import *
from .conftest_db import *
from .conftest_datesets import *
import asyncio


@pytest.fixture(scope="session")
def event_loop():
    return asyncio.get_event_loop()
