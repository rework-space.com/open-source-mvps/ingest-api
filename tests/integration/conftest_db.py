import pytest
from sqlalchemy import create_engine, Engine
from sqlalchemy.orm import Session


@pytest.fixture(scope='session')
def db_engine(pg_url) -> Engine:
    return create_engine(pg_url)


@pytest.fixture()
def db_conn(db_engine):
    with db_engine.connect() as conn:
        yield conn


@pytest.fixture()
def db_session(db_engine):
    with Session(db_engine) as session:
        yield session
