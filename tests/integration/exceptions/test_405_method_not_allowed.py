from pytest_lazyfixture import lazy_fixture
import pytest

from exceptions.error_codes import ErrorCodes
from tests.integration.utils import is_valid_uuid


# 4.5
@pytest.mark.parametrize('async_app, path, method', [
    (lazy_fixture('ingest_app'), '/api/v1/data', 'get'),
    (lazy_fixture('proxy_app'), '/', 'post'),
    # (lazy_fixture('inner_app'), '/api/v1/data', 'post'),
])
@pytest.mark.asyncio
async def test_405_method_not_allowed(async_app, path, method,
                                      make_auth_headers, db_session):
    request = async_app.build_request(method, path)
    response = await async_app.send(request)
    assert response.status_code == 405
    json_resp = response.json()
    assert len(json_resp['errors']) == 1

    for error in json_resp['errors']:
        assert is_valid_uuid(error['id'])
        assert error['code'] == ErrorCodes.method_not_allowed
        assert 'Method not allowed' in error['message']
