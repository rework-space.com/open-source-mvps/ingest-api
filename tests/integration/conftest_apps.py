import os

import pytest
from httpx import AsyncClient

from apps import create_ingest_app, create_admin_app, create_proxy_app
from config import IngestAppSettings, ProxyAppSettings, AdminAppSettings


@pytest.mark.asyncio
@pytest.fixture(scope='session')
async def ingest_app(async_pg_url, alembic_migrate) -> AsyncClient:
    config = IngestAppSettings(ingest_postgres_conn_url=async_pg_url)

    async with AsyncClient(app=create_ingest_app(config),
                           base_url="http://ingest_app/") as async_app:
        yield async_app


@pytest.mark.asyncio
@pytest.fixture(scope='session')
async def proxy_app(async_pg_url, alembic_migrate,
                    ingest_app_url) -> AsyncClient:
    ingest_app_feed_uri = os.path.join(ingest_app_url,
                                       'api/v1/data/feed')
    config = ProxyAppSettings(proxy_postgres_conn_url=async_pg_url,
                              ingest_app_feed_uri=ingest_app_feed_uri)

    async with AsyncClient(app=create_proxy_app(config),
                           base_url="http://proxy_app/") as async_app:
        yield async_app


@pytest.mark.asyncio
@pytest.fixture(scope='function')
async def admin_app(async_pg_url, alembic_migrate) -> AsyncClient:
    config = AdminAppSettings(admin_postgres_conn_url=async_pg_url)

    async with AsyncClient(app=create_admin_app(config),
                           base_url="http://admin_app/"
                           ) as async_app:
        yield async_app
