import os
import time
from hashlib import md5
from random import choice, randint
from uuid import UUID

from faker import Faker
from sqlalchemy import select, func
from sqlalchemy.orm import Session

from models import DataRow, InvalidRequest

fake = Faker()


def check_if_container_running(container):
    container.reload()
    return container.status == 'running'


def wait_until(condition, *args, interval=0.1, timeout=1):
    start = time.time()
    while time.time() - start < timeout:
        if condition(*args):
            return
        time.sleep(interval)
    raise TimeoutError


def ping_server(host, port):
    response = os.system(f"ping -c 5 {host} -p {port}")

    if response == 0:
        return True
    else:
        return False


def is_valid_uuid(uuid_to_test, version=4):
    """
    Check if uuid_to_test is a valid UUID.

     Parameters
    ----------
    uuid_to_test : str
    version : {1, 2, 3, 4}

     Returns
    -------
    `True` if uuid_to_test is a valid UUID, otherwise `False`.

     Examples
    --------
    >>> is_valid_uuid('c9bf9e57-1685-4c89-bafb-ff5af830be8a')
    True
    >>> is_valid_uuid('c9bf9e58')
    False
    """

    try:
        uuid_obj = UUID(uuid_to_test, version=version)
    except ValueError:
        return False
    return str(uuid_obj) == uuid_to_test


def is_dataset_inserted_to_db(db_session: Session,
                              dataset: dict[str, str | int],
                              user_id: str):
    data_row = db_session.execute(
        select(DataRow).filter(
            DataRow.country == dataset.get('country'),
            DataRow.email == dataset.get('email'),
            DataRow.email_hash_md5_content == dataset.get(
                'email_hash_md5_content'),
            DataRow.firstname == dataset.get('firstname'),
            DataRow.lastname == dataset.get('lastname'),
            DataRow.ip == dataset.get('ip'),
            DataRow.url == dataset.get('url'),
            DataRow.date == dataset.get('date'),
            DataRow.dob == dataset.get('dob'),
            DataRow.gender == dataset.get('gender'),
            DataRow.address1 == dataset.get('address1'),
            DataRow.address2 == dataset.get('address2'),
            DataRow.zip == dataset.get('zip'),
            DataRow.city == dataset.get('city'),
            DataRow.state == dataset.get('state'),
            DataRow.vertical == dataset.get('vertical'),
            DataRow.language == dataset.get('language'),
            DataRow.phone_number1 == dataset.get('phone_number1'),
            DataRow.phone_number2 == dataset.get('phone_number2'),
            DataRow.age == dataset.get('age'),
            DataRow._user_id == user_id
        )
    ).scalars().one_or_none()
    if data_row is not None:
        return True
    return False


def count_data_rows(db_session: Session) -> int:
    return db_session.execute(
        select(
            func.count()
        ).select_from(
            DataRow
        )
    ).scalars().one()


def count_invalid_requests(db_session: Session) -> int:
    return db_session.execute(
        select(
            func.count()
        ).select_from(
            InvalidRequest
        )
    ).scalars().one()


def is_invalid_request_logged(db_session: Session,
                              user_id: str,
                              method: str) -> int:
    invalid_request = db_session.execute(
        select(InvalidRequest).filter(
            InvalidRequest.user_id == user_id,
            func.lower(InvalidRequest.method) == method.lower()
        )
    ).scalars().one_or_none()
    if invalid_request is not None:
        return True
    return False


def create_random_dataset():
    email = fake.email()
    return {
        "country": fake.country(),
        "email": email,
        "email_hash_md5_content": md5(email.encode()).hexdigest(),
        "firstname": fake.first_name(),
        "lastname": fake.last_name(),
        "ip": fake.ipv4(),
        "url": fake.url(),
        "date": str(fake.date_time()),
        "dob": str(fake.date_time()),
        "gender": choice(['M', 'F']),
        "address1": fake.address(),
        "address2": fake.address(),
        "zip": fake.postcode(),
        "city": fake.city(),
        "state": fake.state(),
        "vertical": fake.word(),
        "language": fake.country_code(),  # faker doesn't have
        # lang generator
        "phone_number1": fake.phone_number(),
        "phone_number2": fake.phone_number(),
        "age": str(randint(15, 100)),
    }


def get_dataset_columns():
    return ['country', 'email', 'email_hash_md5_content', 'firstname',
            'lastname', 'ip', 'url', 'date', 'dob', 'gender', 'address1',
            'address2', 'zip', 'city', 'state', 'vertical', 'language',
            'phone_number1', 'phone_number2', 'age']


def get_dataset_optional_columns():
    dataset_columns = get_dataset_columns()
    dataset_columns.remove('email')
    return dataset_columns


def create_improper_dataset():
    dataset = {}
    for column in get_dataset_columns():
        dataset[column] = fake.word()
    dataset['age'] = randint(100, 1000)
    return dataset


def rename_dataset(dataset, aliases):
    dataset = dataset.copy()
    for k, v in aliases.items():
        dataset[v] = dataset.pop(k)
    return dataset
