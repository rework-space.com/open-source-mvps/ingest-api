import pytest
from pytest_lazyfixture import lazy_fixture

from exceptions.error_codes import ErrorCodes
from tests.integration.utils import is_valid_uuid, count_data_rows, \
    is_dataset_inserted_to_db, rename_dataset, get_dataset_optional_columns, \
    count_invalid_requests, is_invalid_request_logged


# 2.5, 2.6, 2.9
@pytest.mark.parametrize('async_app, path, dataset, user, aliases', [
    # proxy '/'
    (lazy_fixture('proxy_app'), '/', lazy_fixture('full_dataset'),
     lazy_fixture('db_user'), {}),
    (lazy_fixture('proxy_app'), '/', lazy_fixture('full_dataset'),
     lazy_fixture('db_user'), {'email': 'Email', 'country': 'C'}),
    *[(lazy_fixture('proxy_app'), '/', lazy_fixture('full_dataset'),
       lazy_fixture('db_user'), {column: f'aliased_{column}'})
      for column in get_dataset_optional_columns()],

    # proxy '/{provider}'
    (lazy_fixture('proxy_app'), lazy_fixture('public_endpoint_path'),
     lazy_fixture('full_dataset'),
     lazy_fixture('db_user_with_public_endpoint'), {}),
    (lazy_fixture('proxy_app'), lazy_fixture('public_endpoint_path'),
     lazy_fixture('full_dataset'),
     lazy_fixture('db_user_with_public_endpoint'),
     {'email': 'Email', 'country': 'C'}),
    *[(lazy_fixture('proxy_app'), lazy_fixture('public_endpoint_path'),
       lazy_fixture('full_dataset'),
       lazy_fixture('db_user_with_public_endpoint'),
       {column: f'aliased_{column}'})
      for column in get_dataset_optional_columns()],
])
@pytest.mark.asyncio
async def test_201_push_with_aliased_columns(async_app, path, dataset,
                                             user, aliases,
                                             add_aliases_to_user,
                                             db_session, make_auth_headers):
    user = add_aliases_to_user(user, aliases)
    headers = make_auth_headers(user)
    renamed_dataset = rename_dataset(dataset, aliases)

    response = await async_app.get(
        path, headers=headers, params=renamed_dataset
    )
    assert response.status_code == 201
    json_resp = response.json()
    assert is_valid_uuid(json_resp['data']['id'])
    assert json_resp['data']['status'] == 'success'
    assert count_data_rows(db_session) == 1
    assert is_dataset_inserted_to_db(db_session, dataset,
                                     user_id=str(user.id))


# 2.7, 2.9
@pytest.mark.parametrize('async_app, path, dataset, user, aliases', [
    # proxy '/'
    (lazy_fixture('proxy_app'), '/', lazy_fixture('full_dataset'),
     lazy_fixture('db_user'), {'country': 'C'}),
    *[(lazy_fixture('proxy_app'), '/', lazy_fixture('full_dataset'),
       lazy_fixture('db_user'), {column: f'aliased_{column}'})
      for column in get_dataset_optional_columns()],

    # proxy '/{provider}'
    (lazy_fixture('proxy_app'), lazy_fixture('public_endpoint_path'),
     lazy_fixture('full_dataset'),
     lazy_fixture('db_user_with_public_endpoint'),
     {'country': 'C'}),
    *[(lazy_fixture('proxy_app'), lazy_fixture('public_endpoint_path'),
       lazy_fixture('full_dataset'),
       lazy_fixture('db_user_with_public_endpoint'),
       {column: f'aliased_{column}'})
      for column in get_dataset_optional_columns()],
])
@pytest.mark.asyncio
async def test_422_push_origin_dataset_to_user_with_aliased_columns(
        async_app, path, dataset,
        user, aliases, add_aliases_to_user,
        db_session, make_auth_headers):
    user = add_aliases_to_user(user, aliases)
    headers = make_auth_headers(user)

    response = await async_app.get(
        path, headers=headers, params=dataset
    )
    assert response.status_code == 422
    json_resp = response.json()
    assert len(json_resp['errors']) == len(aliases)

    for error in json_resp['errors']:
        assert is_valid_uuid(error['id'])
        assert 'Error:' in error['message']
        assert error['code'] == ErrorCodes.extra_values_forbidden
    assert count_data_rows(db_session) == 0
    assert count_invalid_requests(db_session) == 1
    assert is_invalid_request_logged(db_session, user_id=user.id, method='GET')


# 2.8, 2.9
@pytest.mark.parametrize('async_app, path, user', [
    # proxy '/'
    (lazy_fixture('proxy_app'), '/', lazy_fixture('db_user')),
    # proxy '/{provider}'
    (lazy_fixture('proxy_app'), lazy_fixture('public_endpoint_path'),
     lazy_fixture('db_user_with_public_endpoint'))
])
@pytest.mark.asyncio
async def test_422_push_origin_dataset_to_user_with_aliased_email(
        async_app, path, full_dataset, user, add_aliases_to_user,
        db_session, make_auth_headers):
    aliases = {'email': 'EMAIL'}
    user = add_aliases_to_user(user, aliases)
    headers = make_auth_headers(user)

    response = await async_app.get(
        path, headers=headers, params=full_dataset
    )
    assert response.status_code == 422
    json_resp = response.json()
    assert len(json_resp['errors']) == len(aliases) + 1
    expected_errors = [ErrorCodes.missing_required_value,
                       ErrorCodes.extra_values_forbidden]

    for error in json_resp['errors']:
        assert is_valid_uuid(error['id'])
        assert 'Error:' in error['message']
        expected_errors.remove(error['code'])

    assert not expected_errors
    assert count_data_rows(db_session) == 0
    assert count_invalid_requests(db_session) == 1
    assert is_invalid_request_logged(db_session, user_id=user.id, method='GET')
