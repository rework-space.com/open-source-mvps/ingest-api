import os
import time
from datetime import datetime, timedelta
from io import BytesIO

import pandas as pd
import pytest

from exceptions.error_codes import ErrorCodes
from models import DataRow
from tests.integration.utils import is_valid_uuid


@pytest.fixture(autouse=True)
def timezone_set():
    os.environ['TZ'] = 'UTC'
    time.tzset()


@pytest.fixture()
def start_date():
    return datetime.now()


@pytest.fixture
def dataset_creation_interval():
    return timedelta(hours=1)


@pytest.fixture
def get_end_date_for(start_date, dataset_creation_interval):
    def get(datasets_count):
        return start_date + datasets_count * dataset_creation_interval

    return get


@pytest.fixture
def seeded_datasets(db_session, db_user, db_admin_user, make_datasets,
                    start_date, dataset_creation_interval):
    datasets = make_datasets(10)
    data_rows = []
    for dataset in datasets:
        data_row = DataRow(**dataset, user=db_user, _created_at=start_date)
        start_date += dataset_creation_interval
        data_rows.append(data_row)
    db_session.add_all(data_rows)
    db_session.commit()
    return datasets


@pytest.fixture
def expected_csv_columns():
    return ["row_id", "country", "email", "email_hash_md5_content",
            "firstname", "lastname", "ip", "url", "date", "dob", "gender",
            "address1", "address2", "zip", "city", "state", "vertical",
            "language", "phone_number1", "phone_number2", "age", "created_at",
            "data_asset_id"]


@pytest.mark.parametrize('datasets_count_to_retrieve', [
    i for i in range(11)
])
@pytest.mark.asyncio
async def test_200_download_dataset(admin_app, datasets_count_to_retrieve,
                                    db_user, db_admin_user,
                                    db_session, seeded_datasets,
                                    make_auth_headers,
                                    start_date, dataset_creation_interval,
                                    get_end_date_for, expected_csv_columns
                                    ):
    end_date = get_end_date_for(datasets_count_to_retrieve)
    params = {'data_asset_id': db_user.data_asset_id,
              'start_date': start_date,
              'end_date': end_date}

    response = await admin_app.get('/api/v1/data', params=params,
                                   headers=make_auth_headers(db_admin_user))

    df = pd.read_csv(BytesIO(response.read()), dtype=object)
    df = df.sort_values(by='created_at')
    assert list(df.columns) == expected_csv_columns
    assert len(df) == datasets_count_to_retrieve
    for dataset, (i, row) in zip(seeded_datasets, df.iterrows()):
        for k, v in dataset.items():
            assert str(row[k]) == str(v)
        assert row['data_asset_id'] == str(db_user.data_asset_id)


@pytest.mark.parametrize("params_to_exclude", [
    ['start_date'], ['data_asset_id'], ['end_date'],
    ['start_date', 'end_date'], ['start_date', 'data_asset_id'],
    ['end_date', 'data_asset_id'],
])
@pytest.mark.asyncio
async def test_422_missed_required_params(
        admin_app, db_user, db_admin_user, db_session, seeded_datasets,
        make_auth_headers, start_date, dataset_creation_interval,
        get_end_date_for, params_to_exclude
):
    end_date = get_end_date_for(5)
    params = {'data_asset_id': db_user.data_asset_id,
              'start_date': start_date,
              'end_date': end_date}
    for param in params_to_exclude:
        del params[param]

    response = await admin_app.get('/api/v1/data', params=params,
                                   headers=make_auth_headers(db_admin_user))
    assert response.status_code == 422
    json_resp = response.json()

    assert len(json_resp['errors']) == len(params_to_exclude)
    for error in json_resp['errors']:
        assert is_valid_uuid(error['id'])
        assert 'Error: field required (type=value_error.missing)' in \
               error['message']
        assert error['code'] == ErrorCodes.missing_required_value


@pytest.mark.parametrize("improper_params", [
    {'start_date': '08-01-2017'}, {'end_date': '08-01-2017'},
    {'data_asset_id': 'a0561de1-e979-4f6b-96fc-85c69c51566'},
    {'start_date': '08-01-2017', 'end_date': '08-01-2017'},
    {'start_date': '08-01-2017',
     'data_asset_id': 'a0561de1-e979-4f6b-96fc-85c69c51566'},
    {'end_date': '08-01-2017',
     'data_asset_id': 'a0561de1-e979-4f6b-96fc-85c69c51566'},
])
@pytest.mark.asyncio
async def test_422_improper_format(
        admin_app, db_user, db_admin_user, db_session, seeded_datasets,
        make_auth_headers, start_date, dataset_creation_interval,
        get_end_date_for, improper_params
):
    end_date = get_end_date_for(5)
    params = {'data_asset_id': db_user.data_asset_id,
              'start_date': start_date,
              'end_date': end_date}

    for k, v in improper_params.items():
        params[k] = v

    response = await admin_app.get('/api/v1/data', params=params,
                                   headers=make_auth_headers(db_admin_user))
    assert response.status_code == 422
    json_resp = response.json()

    assert len(json_resp['errors']) == len(improper_params)
    for error in json_resp['errors']:
        assert is_valid_uuid(error['id'])
        assert 'Error: ' in error['message']
