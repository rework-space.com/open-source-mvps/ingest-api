import pytest
from sqlalchemy import inspect, text


@pytest.fixture(scope='function', autouse=True)
def truncate_db(db_engine):
    inspector = inspect(db_engine)
    schemas = inspector.get_schema_names()
    ignored_schemas = ('public', 'information_schema')
    with db_engine.begin() as conn:
        for schema in schemas:
            if schema in ignored_schemas:
                continue
            for table in inspector.get_table_names(schema=schema):
                conn.execute(text(f'TRUNCATE "{schema}"."{table}" CASCADE'))
