import json
from itertools import combinations

import pytest
from pytest_lazyfixture import lazy_fixture

from exceptions.error_codes import ErrorCodes
from tests.integration.utils import is_valid_uuid, count_data_rows, \
    is_dataset_inserted_to_db, get_dataset_optional_columns, \
    count_invalid_requests, is_invalid_request_logged


# 1.12, 1.13, 1.14, 1.15
@pytest.mark.parametrize('async_app, path, method, user, maker_options', [
    # full dataset
    (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
     lazy_fixture('db_user'), {'count': 1, 'improper': False}),
    (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
     lazy_fixture('db_user'), {'count': 2, 'improper': False}),
    (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
     lazy_fixture('db_user'), {'count': 3, 'improper': False}),
    (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
     lazy_fixture('db_user'), {'count': 100, 'improper': False}),
    # improper dataset
    (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
     lazy_fixture('db_user'), {'count': 1, 'improper': True}),
    (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
     lazy_fixture('db_user'), {'count': 2, 'improper': True}),
    (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
     lazy_fixture('db_user'), {'count': 3, 'improper': True}),
    (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
     lazy_fixture('db_user'), {'count': 100, 'improper': True}),
    # missed one optional column
    *[(lazy_fixture('ingest_app'), '/api/v1/data', 'post',
       lazy_fixture('db_user'), {'count': 1, 'exclude_columns': [column]}
       ) for column in get_dataset_optional_columns()],
    *[(lazy_fixture('ingest_app'), '/api/v1/data', 'post',
       lazy_fixture('db_user'), {'count': 2, 'exclude_columns': [column]}
       ) for column in get_dataset_optional_columns()],
    *[(lazy_fixture('ingest_app'), '/api/v1/data', 'post',
       lazy_fixture('db_user'), {'count': 10, 'exclude_columns': [column]}
       ) for column in get_dataset_optional_columns()],

    # missed two optional columns
    *[(lazy_fixture('ingest_app'), '/api/v1/data', 'post',
       lazy_fixture('db_user'), {'count': 1, 'exclude_columns': columns}
       ) for columns in combinations(get_dataset_optional_columns(), 2)],
    *[(lazy_fixture('ingest_app'), '/api/v1/data', 'post',
       lazy_fixture('db_user'), {'count': 2, 'exclude_columns': columns}
       ) for columns in combinations(get_dataset_optional_columns(), 2)],
    *[(lazy_fixture('ingest_app'), '/api/v1/data', 'post',
       lazy_fixture('db_user'), {'count': 10, 'exclude_columns': columns}
       ) for columns in combinations(get_dataset_optional_columns(), 2)],
])
@pytest.mark.asyncio
async def test_201_post_datasets(async_app, path, method, user,
                                 maker_options, db_session,
                                 make_auth_headers, make_datasets):
    datasets = make_datasets(**maker_options)
    request = async_app.build_request(method, path,
                                      json={'data': datasets},
                                      headers=make_auth_headers(user))
    response = await async_app.send(request)

    assert response.status_code == 201
    json_resp = response.json()
    assert is_valid_uuid(json_resp['data']['id'])
    assert json_resp['data']['status'] == 'success'
    assert count_data_rows(db_session) == maker_options['count']
    for dataset in datasets:
        assert is_dataset_inserted_to_db(db_session, dataset,
                                         user_id=str(user.id))


@pytest.mark.parametrize('async_app, path, method, user, maker_options', [
    (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
     lazy_fixture('db_user'), {'count': 1, "exclude_columns": ['email'],
                               "exclude_count": 1}),
    (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
     lazy_fixture('db_user'), {'count': 2, "exclude_columns": ['email'],
                               "exclude_count": 2}),
    (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
     lazy_fixture('db_user'), {'count': 3, "exclude_columns": ['email'],
                               "exclude_count": 3}),
    (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
     lazy_fixture('db_user'), {'count': 100, "exclude_columns": ['email'],
                               "exclude_count": 40}),
])
@pytest.mark.asyncio
async def test_422_post_without_email(async_app, path, method, user,
                                      maker_options, db_session,
                                      make_auth_headers, make_datasets):
    datasets = make_datasets(**maker_options)
    request = async_app.build_request(method, path,
                                      json={'data': datasets},
                                      headers=make_auth_headers(user))
    response = await async_app.send(request)
    assert response.status_code == 422
    json_resp = response.json()

    assert len(json_resp['errors']) == maker_options['exclude_count']
    for error in json_resp['errors']:
        assert is_valid_uuid(error['id'])
        assert 'email. Error: field required (type=value_error.missing)' in \
               error['message']
        assert error['code'] == ErrorCodes.missing_required_value
    assert count_data_rows(db_session) == 0
    assert count_invalid_requests(db_session) == 1
    assert is_invalid_request_logged(db_session, user_id=user.id,
                                     method=method)


@pytest.mark.parametrize('async_app, path, method, user', [
    (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
     lazy_fixture('db_user')),
])
@pytest.mark.asyncio
async def test_422_post_empty_data_list(async_app, path, method, user,
                                        make_auth_headers, db_session):
    request = async_app.build_request(method, path,
                                      json={'data': []},
                                      headers=make_auth_headers(user))
    response = await async_app.send(request)
    assert response.status_code == 422
    json_resp = response.json()
    assert len(json_resp['errors']) == 1

    for error in json_resp['errors']:
        assert is_valid_uuid(error['id'])
        assert 'Error:' in error['message']
        assert error['code'] == ErrorCodes.empty_list_forbidden
    assert count_data_rows(db_session) == 0
    assert count_invalid_requests(db_session) == 1
    assert is_invalid_request_logged(db_session, user_id=user.id,
                                     method=method)


@pytest.mark.parametrize('async_app, path, method, user', [
    (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
     lazy_fixture('db_user')),
])
@pytest.mark.asyncio
async def test_422_post_with_null_data_value(async_app, path, method, user,
                                             make_auth_headers, db_session):
    request = async_app.build_request(method, path,
                                      json={'data': None},
                                      headers=make_auth_headers(user))
    response = await async_app.send(request)
    assert response.status_code == 422
    json_resp = response.json()
    assert len(json_resp['errors']) == 1

    for error in json_resp['errors']:
        assert is_valid_uuid(error['id'])
        assert 'Error:' in error['message']
        assert error['code'] == ErrorCodes.type_error
    assert count_data_rows(db_session) == 0
    assert count_invalid_requests(db_session) == 1
    assert is_invalid_request_logged(db_session, user_id=user.id,
                                     method=method)


@pytest.mark.parametrize('async_app, path, method, user, maker_options', [
    (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
     lazy_fixture('db_user'), {'count': 1,
                               'extra_options': {'extra1': 'val1'}}),
    (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
     lazy_fixture('db_user'), {'count': 1, 'extra_options': {
        'extra1': 'val1', 'extra2': 'val2', 'extra3': 'val3'}}),
    (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
     lazy_fixture('db_user'), {'count': 2,
                               'extra_options': {'extra1': 'val1'}}),
    (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
     lazy_fixture('db_user'), {'count': 2, 'extra_options': {
        'extra1': 'val1', 'extra2': 'val2', 'extra3': 'val3'}}),
    (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
     lazy_fixture('db_user'), {'count': 10,
                               'extra_options': {'extra1': 'val1'}}),
    (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
     lazy_fixture('db_user'), {'count': 10, 'extra_options': {
        'extra1': 'val1', 'extra2': 'val2', 'extra3': 'val3'}}),
])
@pytest.mark.asyncio
async def test_422_post_with_extra(async_app, path, method, user,
                                   maker_options, make_datasets,
                                   make_auth_headers, db_session):
    datasets = make_datasets(**maker_options)
    request = async_app.build_request(method, path,
                                      json={'data': datasets},
                                      headers=make_auth_headers(user))
    response = await async_app.send(request)
    assert response.status_code == 422
    json_resp = response.json()
    assert len(json_resp['errors']) == (maker_options['count']
                                        * len(maker_options['extra_options']))

    for error in json_resp['errors']:
        assert is_valid_uuid(error['id'])
        assert 'Error: extra fields not permitted' in error['message']
        assert error['code'] == ErrorCodes.extra_values_forbidden
    assert count_data_rows(db_session) == 0
    assert count_invalid_requests(db_session) == 1
    assert is_invalid_request_logged(db_session, user_id=user.id,
                                     method=method)


@pytest.mark.parametrize('async_app, path, method, user, maker_options', [
    (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
     lazy_fixture('db_user'), {'count': 2}),
])
@pytest.mark.asyncio
async def test_422_post_invalid_json(async_app, path, method, user,
                                     maker_options, make_datasets,
                                     make_auth_headers, db_session):
    datasets = make_datasets(**maker_options)
    request_content = json.dumps({'data': datasets})
    request_content = request_content[:-2]  # corrupt json
    request = async_app.build_request(method, path,
                                      content=request_content,
                                      headers=make_auth_headers(user))
    response = await async_app.send(request)
    assert response.status_code == 422
    json_resp = response.json()
    assert len(json_resp['errors']) == 1

    assert is_valid_uuid(json_resp['errors'][0]['id'])
    assert 'Error:' in json_resp['errors'][0]['message']
    assert json_resp['errors'][0]['code'] == ErrorCodes.invalid_json
    assert count_data_rows(db_session) == 0
    assert count_invalid_requests(db_session) == 1
    assert is_invalid_request_logged(db_session, user_id=user.id,
                                     method=method)


@pytest.mark.parametrize(
    'async_app, path, method, user, maker_options, extra_fields_in_root', [
        (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
         lazy_fixture('db_user'), {'count': 2}, {'extra1': 'val1'}),
        (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
         lazy_fixture('db_user'), {'count': 2}, {'extra1': 'val1',
                                                 'extra2': 'val2'}),
        (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
         lazy_fixture('db_user'), {'count': 2}, {'extra1': 'val1',
                                                 'extra2': 'val2'}),
    ])
@pytest.mark.asyncio
async def test_422_post_with_extra_in_root(async_app, path, method, user,
                                           maker_options, extra_fields_in_root,
                                           make_datasets,
                                           make_auth_headers, db_session):
    datasets = make_datasets(**maker_options)
    request = async_app.build_request(method, path,
                                      json={'data': datasets,
                                            **extra_fields_in_root},
                                      headers=make_auth_headers(user))
    response = await async_app.send(request)
    assert response.status_code == 422
    json_resp = response.json()
    assert len(json_resp['errors']) == len(extra_fields_in_root)
    for error in json_resp['errors']:
        assert is_valid_uuid(error['id'])
        assert 'Error: extra fields not permitted' in error['message']
        assert error['code'] == ErrorCodes.extra_values_forbidden
    assert count_data_rows(db_session) == 0
    assert count_invalid_requests(db_session) == 1
    assert is_invalid_request_logged(db_session, user_id=user.id,
                                     method=method)
