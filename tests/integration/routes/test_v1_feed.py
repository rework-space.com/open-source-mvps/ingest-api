import re
from itertools import combinations

import pytest
from pytest_lazyfixture import lazy_fixture

from exceptions.error_codes import ErrorCodes
from tests.integration.utils import is_valid_uuid, is_dataset_inserted_to_db, \
    count_data_rows, get_dataset_optional_columns, count_invalid_requests, \
    is_invalid_request_logged


# 1.4, 1.5, 2.1, 2.2
@pytest.mark.parametrize('async_app, path, dataset, user, send_api_key', [
    # ingest /api/v1/data/feed
    (lazy_fixture('ingest_app'), 'api/v1/data/feed',
     lazy_fixture('full_dataset'), lazy_fixture('db_user'),
     True),
    (lazy_fixture('ingest_app'), 'api/v1/data/feed',
     lazy_fixture('improper_dataset'), lazy_fixture('db_user'),
     True),
    # proxy '/'
    (lazy_fixture('proxy_app'), '/',
     lazy_fixture('full_dataset'),
     lazy_fixture('db_user_with_aliases'),
     True),
    (lazy_fixture('proxy_app'), '/',
     lazy_fixture('improper_dataset'), lazy_fixture('db_user_with_aliases'),
     True),
    # proxy '/{provider}'
    (lazy_fixture('proxy_app'), lazy_fixture('public_endpoint_path'),
     lazy_fixture('full_dataset'),
     lazy_fixture('db_user_with_public_endpoint'),
     False),
    (lazy_fixture('proxy_app'), lazy_fixture('public_endpoint_path'),
     lazy_fixture('improper_dataset'),
     lazy_fixture('db_user_with_public_endpoint'),
     False),
])
@pytest.mark.asyncio
async def test_push_full_data_set(async_app, path, dataset, user, send_api_key,
                                  db_session, make_auth_headers):
    headers = make_auth_headers(user) if send_api_key else None

    response = await async_app.get(
        path, headers=headers, params=dataset
    )
    assert response.status_code == 201
    json_resp = response.json()
    assert is_valid_uuid(json_resp['data']['id'])
    assert json_resp['data']['status'] == 'success'
    assert count_data_rows(db_session) == 1
    assert is_dataset_inserted_to_db(db_session, dataset,
                                     user_id=str(user.id))


# 1.6, 1.7, 2.1, 2.2
@pytest.mark.parametrize(
    'async_app, path, columns_to_exclude, user, send_api_key', [
        # ingest /api/v1/data/feed
        *[(lazy_fixture('ingest_app'), 'api/v1/data/feed',
           [column],
           lazy_fixture('db_user'), True)
          for column in get_dataset_optional_columns()],
        # all 2 columns combinations
        *[(lazy_fixture('ingest_app'), 'api/v1/data/feed',
           columns,
           lazy_fixture('db_user'), True)
          for columns in
          combinations(get_dataset_optional_columns(), 2)],
        (lazy_fixture('ingest_app'), 'api/v1/data/feed',
         ['date', 'firstname',
          'lastname', 'country'],
         lazy_fixture('db_user'), True),

        # proxy '/'
        *[(lazy_fixture('proxy_app'), '/', [column],
           lazy_fixture('db_user_with_aliases'), True)
          for column in get_dataset_optional_columns()],
        # all 2 columns combinations
        *[(lazy_fixture('proxy_app'), '/', columns,
           lazy_fixture('db_user_with_aliases'), True)
          for columns in
          combinations(get_dataset_optional_columns(), 2)],
        (lazy_fixture('proxy_app'), '/',
         ['date', 'firstname', 'lastname',
          'country'],
         lazy_fixture('db_user_with_aliases'), True),

        # proxy '/{provider}'
        *[(lazy_fixture('proxy_app'),
           lazy_fixture('public_endpoint_path'), [column],
           lazy_fixture('db_user_with_public_endpoint'),
           False)
          for column in get_dataset_optional_columns()],
        # all 2 columns combinations
        *[(lazy_fixture('proxy_app'),
           lazy_fixture('public_endpoint_path'), columns,
           lazy_fixture('db_user_with_public_endpoint'),
           False)
          for columns in
          combinations(get_dataset_optional_columns(), 2)],
        (lazy_fixture('proxy_app'),
         lazy_fixture('public_endpoint_path'),
         ['date', 'firstname', 'lastname', 'country'],
         lazy_fixture('db_user_with_public_endpoint'),
         False),
    ])
@pytest.mark.asyncio
async def test_push_different_dataset_combos(
        async_app, path, columns_to_exclude, user, send_api_key, db_session,
        make_auth_headers, get_full_dataset_with_excluded_options
):
    headers = make_auth_headers(user) if send_api_key else None

    dataset = get_full_dataset_with_excluded_options(*columns_to_exclude)
    response = await async_app.get(path, headers=headers,
                                   params=dataset)
    assert response.status_code == 201
    json_resp = response.json()
    assert is_valid_uuid(json_resp['data']['id'])
    assert json_resp['data']['status'] == 'success'
    assert count_data_rows(db_session) == 1
    assert is_dataset_inserted_to_db(db_session, dataset,
                                     user_id=str(user.id))


# 1.8, 1.9, 2.1, 2.2
@pytest.mark.parametrize('async_app, path, user, dataset, send_api_key', [
    # ingest /api/v1/data/feed
    (lazy_fixture('ingest_app'), 'api/v1/data/feed', lazy_fixture('db_user'),
     lazy_fixture('full_dataset_without_email'), True),
    (lazy_fixture('ingest_app'), 'api/v1/data/feed', lazy_fixture('db_user'),
     lazy_fixture('empty_dataset'), True),
    # proxy '/'
    (lazy_fixture('proxy_app'), '/', lazy_fixture('db_user_with_aliases'),
     lazy_fixture('full_dataset_without_email'), True),
    (lazy_fixture('proxy_app'), '/', lazy_fixture('db_user_with_aliases'),
     lazy_fixture('empty_dataset'), True),
    # proxy '/{provider}'
    (lazy_fixture('proxy_app'), lazy_fixture('public_endpoint_path'),
     lazy_fixture('db_user_with_public_endpoint'),
     lazy_fixture('full_dataset_without_email'),
     False),
    (lazy_fixture('proxy_app'), lazy_fixture('public_endpoint_path'),
     lazy_fixture('db_user_with_public_endpoint'),
     lazy_fixture('empty_dataset'),
     False),
])
@pytest.mark.asyncio
async def test_422_push_dataset_without_email(async_app, path, user,
                                              send_api_key,
                                              db_session, make_auth_headers,
                                              dataset):
    headers = make_auth_headers(user) if send_api_key else None

    response = await async_app.get(path, headers=headers, params=dataset)
    assert response.status_code == 422
    json_resp = response.json()

    assert len(json_resp['errors']) == 1
    assert is_valid_uuid(json_resp['errors'][0]['id'])
    assert 'Loc: email. Error: field required (type=value_error.missing)' in \
           json_resp['errors'][0]['message']
    assert json_resp['errors'][0]['code'] == ErrorCodes.missing_required_value

    assert count_data_rows(db_session) == 0
    assert count_invalid_requests(db_session) == 1
    assert is_invalid_request_logged(db_session, user_id=user.id,
                                     method='get')


# 1.10, 2.1, 2.2
@pytest.mark.parametrize('async_app, path, extra_options, user, send_api_key', [  # noqa: E501
    # ingest /api/v1/data/feed
    (lazy_fixture('ingest_app'), 'api/v1/data/feed',
     {'extra1': 'value1'},
     lazy_fixture('db_user'), True),
    (lazy_fixture('ingest_app'), 'api/v1/data/feed',
     {'extra1': 'value1',
      'extra2': 'value2'},
     lazy_fixture('db_user'), True),
    (lazy_fixture('ingest_app'), 'api/v1/data/feed',
     {'extra1': 'value1',
      'extra2': 'value2',
      'extra3': 'value3'},
     lazy_fixture('db_user'), True),
    # proxy '/'
    (lazy_fixture('proxy_app'), '/',
     {'extra1': 'value1'},
     lazy_fixture('db_user_with_aliases'), True),
    (lazy_fixture('proxy_app'), '/',
     {'extra1': 'value1',
      'extra2': 'value2'},
     lazy_fixture('db_user_with_aliases'), True),
    (lazy_fixture('proxy_app'), '/',
     {'extra1': 'value1', 'extra2': 'value2',
      'extra3': 'value3'},
     lazy_fixture('db_user_with_aliases'), True),
    # proxy '/{provider}'
    (lazy_fixture('proxy_app'),
     lazy_fixture('public_endpoint_path'),
     {'extra1': 'value1'},
     lazy_fixture('db_user_with_public_endpoint'),
     False),
    (lazy_fixture('proxy_app'),
     lazy_fixture('public_endpoint_path'),
     {'extra1': 'value1', 'extra2': 'value2'},
     lazy_fixture('db_user_with_public_endpoint'),
     False),
    (lazy_fixture('proxy_app'),
     lazy_fixture('public_endpoint_path'),
     {'extra1': 'value1', 'extra2': 'value2',
      'extra3': 'value3'},
     lazy_fixture('db_user_with_public_endpoint'),
     False),
])
@pytest.mark.asyncio
async def test_push_dataset_with_extra_and_missed_email(
        async_app, path, extra_options, user, send_api_key,
        db_session, make_auth_headers,
        get_dataset_without_email_and_with_extra_options):
    headers = make_auth_headers(user) if send_api_key else None
    dataset = get_dataset_without_email_and_with_extra_options(**extra_options)

    response = await async_app.get(path, headers=headers, params=dataset)
    assert response.status_code == 422
    json_resp = response.json()

    # + 1 email missed error
    assert len(json_resp['errors']) == len(extra_options) + 1

    # assert email error
    for i, error in enumerate(json_resp['errors']):
        if 'Loc: email. Error: field required (type=value_error.missing)' in \
                error['message']:
            assert is_valid_uuid(error['id'])
            assert error['code'] == ErrorCodes.missing_required_value
            break
    else:
        assert False, 'Error about missed email was not found'
    del json_resp['errors'][i]

    expected_extra_options_names = list(extra_options.keys())
    for i, *_ in enumerate(extra_options):
        assert is_valid_uuid(json_resp['errors'][i]['id'])
        assert json_resp['errors'][i]['code'] == \
               ErrorCodes.extra_values_forbidden

        res = re.match('Loc: (.*). Error: extra fields not permitted.*',
                       json_resp['errors'][i]['message'])
        assert res
        expected_extra_options_names.remove(res.groups()[0])

    assert len(expected_extra_options_names) == 0
    assert count_data_rows(db_session) == 0
    assert count_invalid_requests(db_session) == 1
    assert is_invalid_request_logged(db_session, user_id=user.id,
                                     method='get')


# 1.11, 2.1. 2.2
@pytest.mark.parametrize('async_app, path, extra_options, user, send_api_key', [  # noqa: E501
    # ingest /api/v1/data/feed
    (lazy_fixture('ingest_app'), 'api/v1/data/feed',
     {'extra1': 'value1'},
     lazy_fixture('db_user'), True),
    (lazy_fixture('ingest_app'), 'api/v1/data/feed',
     {'extra1': 'value1',
      'extra2': 'value2'},
     lazy_fixture('db_user'), True),
    (lazy_fixture('ingest_app'), 'api/v1/data/feed',
     {'extra1': 'value1',
      'extra2': 'value2',
      'extra3': 'value3'},
     lazy_fixture('db_user'), True),
    # proxy '/'
    (lazy_fixture('proxy_app'), '/',
     {'extra1': 'value1'},
     lazy_fixture('db_user_with_aliases'), True),
    (lazy_fixture('proxy_app'), '/',
     {'extra1': 'value1',
      'extra2': 'value2'},
     lazy_fixture('db_user_with_aliases'), True),
    (lazy_fixture('proxy_app'), '/',
     {'extra1': 'value1', 'extra2': 'value2',
      'extra3': 'value3'},
     lazy_fixture('db_user_with_aliases'), True),
    # proxy '/{provider}'
    (lazy_fixture('proxy_app'),
     lazy_fixture('public_endpoint_path'),
     {'extra1': 'value1'},
     lazy_fixture('db_user_with_public_endpoint'),
     False),
    (lazy_fixture('proxy_app'),
     lazy_fixture('public_endpoint_path'),
     {'extra1': 'value1', 'extra2': 'value2'},
     lazy_fixture('db_user_with_public_endpoint'),
     False),
    (lazy_fixture('proxy_app'),
     lazy_fixture('public_endpoint_path'),
     {'extra1': 'value1', 'extra2': 'value2',
      'extra3': 'value3'},
     lazy_fixture('db_user_with_public_endpoint'),
     False),
])
@pytest.mark.asyncio
async def test_push_dataset_with_extra(
        async_app, path, extra_options, user, send_api_key, make_auth_headers,
        get_full_dataset_with_extra_options, db_session):
    headers = make_auth_headers(user) if send_api_key else None

    dataset = get_full_dataset_with_extra_options(**extra_options)

    response = await async_app.get(path, headers=headers, params=dataset)
    assert response.status_code == 422
    json_resp = response.json()

    assert len(json_resp['errors']) == len(extra_options)

    expected_extra_options_names = list(extra_options.keys())
    for i, *_ in enumerate(extra_options):
        assert is_valid_uuid(json_resp['errors'][i]['id'])
        assert json_resp['errors'][i]['code'] == \
               ErrorCodes.extra_values_forbidden
        res = re.match('Loc: (.*). Error: extra fields not permitted.*',
                       json_resp['errors'][i]['message'])
        assert res
        expected_extra_options_names.remove(res.groups()[0])
    assert len(expected_extra_options_names) == 0
    assert count_data_rows(db_session) == 0
    assert count_invalid_requests(db_session) == 1
    assert is_invalid_request_logged(db_session, user_id=user.id,
                                     method='get')
