import pytest
from pytest_lazyfixture import lazy_fixture

from tests.integration.utils import is_valid_uuid
from exceptions.error_codes import ErrorCodes


parametrize_options = [
    (lazy_fixture('ingest_app'), '/api/v1/data/feed', 'get',
     lazy_fixture('db_user')),
    (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
     lazy_fixture('db_user')),
    # proxy app
    (lazy_fixture('proxy_app'), '/', 'get',
     lazy_fixture('db_user')),
    (lazy_fixture('proxy_app'), '/', 'get',
     lazy_fixture('db_user_with_aliases')),
    # inner app
    (lazy_fixture('admin_app'), '/api/v1/data', 'get',
     lazy_fixture('db_admin_user')),
]


# 1.1, 2.1, 3.1
@pytest.mark.parametrize('async_app, path, method, user', parametrize_options)
@pytest.mark.asyncio
async def test_auth_with_empty_key_header(async_app, path, method, user,
                                          make_auth_headers):
    request = async_app.build_request(method, path)
    response = await async_app.send(request)

    assert response.status_code == 401
    json_resp = response.json()
    assert len(json_resp['errors']) == 1
    assert is_valid_uuid(json_resp['errors'][0]['id'])
    assert json_resp['errors'][0]['code'] == ErrorCodes.key_not_provided
    assert 'Authentication information is missing or invalid' \
           in json_resp['errors'][0]['message']


# 1.2, 2.1, 3.1
@pytest.mark.parametrize('async_app, path, method, user', parametrize_options)
@pytest.mark.asyncio
async def test_auth_with_invalid_key(async_app, path, method, user,
                                     make_auth_headers):
    auth_headers = make_auth_headers(user)
    # corrupt the token
    auth_headers['X-API-KEY'] = auth_headers['X-API-KEY'][:-1]
    request = async_app.build_request(method, path, headers=auth_headers)
    response = await async_app.send(request)
    assert response.status_code == 401
    json_resp = response.json()
    assert len(json_resp['errors']) == 1
    assert is_valid_uuid(json_resp['errors'][0]['id'])
    assert json_resp['errors'][0]['code'] == ErrorCodes.user_not_found
    assert 'Authentication information is missing or invalid' \
           in json_resp['errors'][0]['message']


# 1.3, 2.1, 3.1
@pytest.mark.parametrize('async_app, path, method, user', [
    # ingest
    (lazy_fixture('ingest_app'), '/api/v1/data/feed', 'get',
     lazy_fixture('db_user')),
    (lazy_fixture('ingest_app'), '/api/v1/data', 'post',
     lazy_fixture('db_user')),\
    # proxy
    (lazy_fixture('proxy_app'), '/', 'get',
     lazy_fixture('db_user_with_aliases')),
    # inner
    (lazy_fixture('admin_app'), '/api/v1/data', 'get',
     lazy_fixture('db_admin_user')),
])
async def test_auth_with_disabled_user(async_app, path, method, user,
                                       make_auth_headers, disable_db_user):
    disable_db_user(user)
    auth_headers = make_auth_headers(user)

    request = async_app.build_request(method, path,
                                      headers=auth_headers)
    response = await async_app.send(request)
    assert response.status_code == 401
    json_resp = response.json()
    assert len(json_resp['errors']) == 1
    assert is_valid_uuid(json_resp['errors'][0]['id'])
    assert json_resp['errors'][0]['code'] == ErrorCodes.user_not_active
    assert 'Authentication information is missing or invalid' \
           in json_resp['errors'][0]['message']


# 2.3
@pytest.mark.parametrize('async_app, path, method, user, send_api_key', [
    (lazy_fixture('proxy_app'), '/non-existing', 'get',
     lazy_fixture('db_user'), True),
    (lazy_fixture('proxy_app'), '/non-existing', 'get',
     lazy_fixture('db_user'), False),
])
async def test_auth_push_to_non_existing_public_path(
        async_app, path, method, user, send_api_key, make_auth_headers
):
    headers = make_auth_headers(user) if send_api_key else None
    request = async_app.build_request(method, path,
                                      headers=headers)
    response = await async_app.send(request)

    assert response.status_code == 404
    json_resp = response.json()
    assert len(json_resp['errors']) == 1
    assert is_valid_uuid(json_resp['errors'][0]['id'])
    assert json_resp['errors'][0]['code'] == ErrorCodes.not_found
    assert 'Not found' in json_resp['errors'][0]['message']


# 2.4
@pytest.mark.parametrize('async_app, path, method, user, user_mutator, send_api_key', [  # noqa: E501
    # disabled user
    (lazy_fixture('proxy_app'), lazy_fixture('public_endpoint_path'), 'get',
     lazy_fixture('db_user_with_public_endpoint'),
     lazy_fixture('disable_db_user'), True),
    (lazy_fixture('proxy_app'), lazy_fixture('public_endpoint_path'), 'get',
     lazy_fixture('db_user_with_public_endpoint'),
     lazy_fixture('disable_db_user'), False),
    # disabled public endpoint
    (lazy_fixture('proxy_app'), lazy_fixture('public_endpoint_path'), 'get',
     lazy_fixture('db_user_with_public_endpoint'),
     lazy_fixture('disable_db_user_public_endpoint'), True),
    (lazy_fixture('proxy_app'), lazy_fixture('public_endpoint_path'), 'get',
     lazy_fixture('db_user_with_public_endpoint'),
     lazy_fixture('disable_db_user_public_endpoint'), False),
    # disable user and public endpoint
    (lazy_fixture('proxy_app'), lazy_fixture('public_endpoint_path'), 'get',
     lazy_fixture('db_user_with_public_endpoint'),
     lazy_fixture('disable_db_user_and_public_endpoint'), True),
    (lazy_fixture('proxy_app'), lazy_fixture('public_endpoint_path'), 'get',
     lazy_fixture('db_user_with_public_endpoint'),
     lazy_fixture('disable_db_user_and_public_endpoint'), False),
])
async def test_auth_push_to_disabled_user_or_public_endpoint(
        async_app, path, method, user, user_mutator, send_api_key,
        make_auth_headers
):
    user_mutator(user)
    headers = make_auth_headers(user) if send_api_key else None
    request = async_app.build_request(method, path,
                                      headers=headers)
    response = await async_app.send(request)

    assert response.status_code == 404
    json_resp = response.json()
    assert len(json_resp['errors']) == 1
    assert is_valid_uuid(json_resp['errors'][0]['id'])
    assert json_resp['errors'][0]['code'] == ErrorCodes.not_found
    assert 'Not found' in json_resp['errors'][0]['message']
