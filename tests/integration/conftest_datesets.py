import pytest

from tests.integration.utils import create_random_dataset, \
    create_improper_dataset


@pytest.fixture
def full_dataset():
    return create_random_dataset()


@pytest.fixture
def improper_dataset():
    return create_improper_dataset()


@pytest.fixture
def full_dataset_without_email(full_dataset):
    del full_dataset['email']
    return full_dataset


@pytest.fixture
def empty_dataset():
    return {}


@pytest.fixture
def get_full_dataset_with_excluded_options():
    def exclude(*columns):
        dataset = create_random_dataset()
        for column in columns:
            del dataset[column]
        return dataset

    return exclude


@pytest.fixture
def get_full_dataset_with_extra_options():
    def add_extra(**extra):
        dataset = create_random_dataset()
        for name, value in extra.items():
            dataset[name] = value
        return dataset

    return add_extra


@pytest.fixture
def get_dataset_without_email_and_with_extra_options():
    def add_extra(**extra):
        dataset = create_random_dataset()
        del dataset['email']
        for name, value in extra.items():
            dataset[name] = value
        return dataset

    return add_extra


@pytest.fixture
def make_datasets():
    def make(count, improper=False,
             exclude_columns: list[str] | None = None,
             exclude_count: int | None = None,
             extra_options: dict[str, str] | None = None,
             datasets_with_extra_count: int | None = None):
        if exclude_count is None:
            exclude_count = count
        if datasets_with_extra_count is None:
            datasets_with_extra_count = count
        if improper:
            maker = create_improper_dataset
        else:
            maker = create_random_dataset

        excluded_count = 0
        cur_datasets_with_extra_count = 0
        datasets = []
        for _ in range(count):
            dataset = maker()
            # exclude column
            if exclude_columns and excluded_count < exclude_count:
                for column in exclude_columns:
                    del dataset[column]
                excluded_count += 1
            datasets.append(dataset)
            # add extra
            if extra_options and \
                    cur_datasets_with_extra_count < datasets_with_extra_count:
                for name, value in extra_options.items():
                    dataset[name] = value
                cur_datasets_with_extra_count += 1

        return datasets
    return make
