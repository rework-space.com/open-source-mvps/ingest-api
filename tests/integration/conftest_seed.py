from typing import Callable
from uuid import uuid4

import pytest

from models import User, UserConfig, ProxyConfig, AliasesConfig, \
    PublicEndpointConfig, AdminUser


@pytest.fixture
def db_user(db_session) -> User:
    user = User(
        data_asset_id=uuid4(),
        enable=True,
        config=UserConfig(),
    )
    db_session.add(user)
    db_session.commit()
    return user


@pytest.fixture
def db_admin_user(db_session) -> User:
    user = AdminUser(
        enable=True,
    )
    db_session.add(user)
    db_session.commit()
    return user


@pytest.fixture
def db_user_with_aliases(db_session) -> User:
    user = User(
        data_asset_id=uuid4(),
        enable=True,
        config=UserConfig(
            proxy_config=ProxyConfig(
                aliases_config=AliasesConfig()
            )
        ),
    )
    db_session.add(user)
    db_session.commit()
    return user


@pytest.fixture
def db_user_with_public_endpoint(db_session) -> User:
    user = User(
        data_asset_id=uuid4(),
        enable=True,
        config=UserConfig(
            proxy_config=ProxyConfig(
                aliases_config=AliasesConfig(),
                public_endpoint_config=PublicEndpointConfig(
                    enable=True,
                    path_parameter='testPathParameter',
                )
            )
        ),
    )
    db_session.add(user)
    db_session.commit()
    return user


@pytest.fixture
def public_endpoint_path(db_user_with_public_endpoint):
    path = db_user_with_public_endpoint.config.proxy_config \
        .public_endpoint_config.path_parameter
    return f'/{path}'


@pytest.fixture
def disable_db_user(db_session) -> Callable[[User], None]:
    def disable(db_user: User):
        db_user.enable = False
        db_session.add(db_user)
        db_session.commit()

    return disable


@pytest.fixture
def disable_db_user_public_endpoint(db_session) -> Callable[[User], None]:
    def disable(db_user: User):
        db_user.config.proxy_config.public_endpoint_config.enable = False
        db_session.add(db_user)
        db_session.commit()

    return disable


@pytest.fixture
def disable_db_user_and_public_endpoint(db_session) -> Callable[[User], None]:
    def disable(db_user: User):
        db_user.config.proxy_config.public_endpoint_config.enable = False
        db_user.enable = False
        db_session.add(db_user)
        db_session.commit()

    return disable


@pytest.fixture
def add_aliases_to_user(db_session):
    def make(user, aliases: dict):
        if user.config.proxy_config:
            user.config.proxy_config.aliases_config = AliasesConfig(**aliases)
        else:
            user.config.proxy_config = ProxyConfig(
                aliases_config=AliasesConfig(**aliases)
            )
        db_session.add(user)
        db_session.commit()
        return user

    return make


@pytest.fixture
def make_auth_headers() -> Callable[[User], dict]:
    def make(user: User):
        return {'X-API-KEY': user.api_key}

    return make
