import subprocess

import docker
import pytest
from sqlalchemy_wrapper import Alembic
from sqlalchemy_wrapper import SQLAlchemy

from models import registry
from tests.integration.utils import wait_until, check_if_container_running, \
    ping_server


@pytest.fixture(scope='session')
def docker_client() -> docker.DockerClient:
    return docker.from_env()


@pytest.fixture(scope='session')
def up_postgres_server(docker_client) -> str:
    """Returns postgres connection url"""
    container = docker_client.containers.run(
        image='postgres:13.4',
        environment={
            "POSTGRES_USER": 'admin',
            "POSTGRES_PASSWORD": 'admin',
        },
        ports={
            '5432': '54321',
        },
        remove=True,
        detach=True,
    )
    wait_until(check_if_container_running, container, interval=1, timeout=30)
    wait_until(ping_server, 'localhost', '54321',
               interval=1, timeout=30)
    yield 'postgresql://admin:admin@localhost:54321/admin'
    container.stop()


@pytest.fixture(scope='session')
def pg_url(up_postgres_server):
    """Shortcut for 'up_postgres_server' fixture"""
    yield up_postgres_server


@pytest.fixture(scope='session')
def async_pg_url(up_postgres_server):
    driver, conn = up_postgres_server.split('://')
    driver = 'postgresql+asyncpg'
    return f'{driver}://{conn}'


@pytest.fixture(scope='session')
def up_ingest_app_server(docker_client, async_pg_url,
                         pytestconfig) -> str:
    image_name = 'test_ingest_app_image'
    subprocess.check_call(['docker', 'build', '-t',
                           f'{image_name}:latest',
                           str(pytestconfig.rootpath)])

    container = docker_client.containers.run(
        image=image_name,
        environment={
            "INGEST_POSTGRES_CONN_URL": async_pg_url,
        },
        command='main:create_ingest_app --host 0.0.0.0 --port 8080',
        remove=True,
        detach=True,
        network_mode='host',
    )
    wait_until(check_if_container_running, container, interval=1, timeout=30)
    wait_until(ping_server, 'localhost', '8080',
               interval=1, timeout=30)
    yield 'http://localhost:8080'
    container.stop()


@pytest.fixture(scope='session')
def ingest_app_url(up_ingest_app_server):
    """Shortcut for 'up_ingest_app_server' fixture"""
    yield up_ingest_app_server


@pytest.fixture(scope='session')
def alembic_migrate(pg_url):
    alembic = Alembic(db=SQLAlchemy(pg_url),
                      registry=registry)
    alembic.upgrade()
