import os
from functools import lru_cache
from hashlib import md5
from random import choice, randint

from faker import Faker

fake = Faker()


@lru_cache
def create_random_dataset():
    email = fake.email()
    return {
        "country": fake.country(),
        "email": email,
        "email_hash_md5_content": md5(email.encode()).hexdigest(),
        "firstname": fake.first_name(),
        "lastname": fake.last_name(),
        "ip": fake.ipv4(),
        "url": fake.url(),
        "date": str(fake.date_time()),
        "dob": str(fake.date_time()),
        "gender": choice(['M', 'F']),
        "address1": fake.address(),
        "address2": fake.address(),
        "zip": fake.postcode(),
        "city": fake.city(),
        "state": fake.state(),
        "vertical": fake.word(),
        "language": fake.country_code(),  # faker doesn't have
        # lang generator
        "phone_number1": fake.phone_number(),
        "phone_number2": fake.phone_number(),
        "age": str(randint(15, 100)),
    }


@lru_cache()
def get_post_request_body() -> dict:
    data = []
    body = {'data': data}
    for i in range(1670):
        data.append(create_random_dataset())
    return body


def get_env(name):
    api_key = os.environ.get(name)
    if api_key is None:
        raise Exception(f'{name!r} env variable not found')
    return api_key
