from locust import task, constant, FastHttpUser

from tests.load.utils import get_post_request_body, get_env

api_key = get_env("API_KEY")


class PostEndpointUser(FastHttpUser):
    wait_time = constant(0)

    @task
    def test_post_endpoint(self):
        data = get_post_request_body()
        self.client.post("/api/v1/data", name='/api/v1/data', json=data,
                         headers={
                             'x-api-key': api_key
                         })
