from locust import task, constant, FastHttpUser

from utils import create_random_dataset, get_env


api_key = get_env('API_KEY')
public_path = get_env('PUBLIC_PATH')


class ProxyPublicEndpointUser(FastHttpUser):
    wait_time = constant(0)

    @task
    def test_proxy_public_endpoint(self):
        dataset = create_random_dataset()
        self.client.get(f"/{public_path}",
                        name=f'/{public_path}', params=dataset,
                        headers={
                            'x-api-key': api_key
                        })
