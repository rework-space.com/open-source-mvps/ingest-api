from locust import task, constant, FastHttpUser

from tests.load.utils import create_random_dataset, get_env


api_key = get_env("API_KEY")


class FeedEndpointUser(FastHttpUser):
    wait_time = constant(0)

    @task
    def test_feed_endpoint(self):
        dataset = create_random_dataset()
        self.client.get("/api/v1/data/feed",
                        name='/api/v1/data/feed', params=dataset,
                        headers={
                            'x-api-key': api_key
                        })
