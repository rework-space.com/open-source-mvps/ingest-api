from locust import FastHttpUser, task, constant

from tests.load.utils import create_random_dataset, \
    get_post_request_body, get_env

api_key = get_env("API_KEY")
ingest_api_url = get_env('INGEST_API_URL')
ingest_proxy_url = get_env('INGEST_PROXY_URL')
public_path = get_env('PUBLIC_PATH')


class IngestAPIUser(FastHttpUser):
    weight = 3
    wait_time = constant(0)
    host = ingest_api_url

    @task(weight=100)
    def test_feed_endpoint(self):
        dataset = create_random_dataset()
        self.client.get("/api/v1/data/feed",
                        name=f'{ingest_api_url}/api/v1/data/feed',
                        params=dataset,
                        headers={
                            'x-api-key': api_key
                        })

    @task(weight=1)
    def test_post_endpoint(self):
        data = get_post_request_body()
        self.client.post("/api/v1/data",
                         name=f'{ingest_api_url}/api/v1/data', json=data,
                         headers={
                             'x-api-key': api_key
                         })


class IngestProxyUser(FastHttpUser):
    weight = 1
    wait_time = constant(0)
    host = ingest_proxy_url

    @task(weight=100)
    def test_proxy_root_endpoint(self):
        dataset = create_random_dataset()
        self.client.get("/",
                        name=f'{ingest_proxy_url}/', params=dataset,
                        headers={
                            'x-api-key': api_key
                        })

    @task(weight=70)
    def test_proxy_public_endpoint(self):
        dataset = create_random_dataset()
        self.client.get(f"/{public_path}",
                        name=f'{ingest_proxy_url}/{public_path}',
                        params=dataset)
