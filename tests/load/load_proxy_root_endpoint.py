from locust import task, constant, FastHttpUser

from utils import create_random_dataset, get_env


api_key = get_env('API_KEY')


class ProxyRootEndpointUser(FastHttpUser):
    wait_time = constant(0)

    @task
    def test_proxy_root_endpoint(self):
        dataset = create_random_dataset()
        self.client.get("/",
                        name='/', params=dataset,
                        headers={
                            'x-api-key': api_key
                        })
