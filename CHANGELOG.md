<a name="0.3.2"></a>
## [0.3.2] - 28-07-2023
### Changed
- the term name data source to data asset ([64be67f](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/64be67f))

<a name="0.3.1"></a>
## [0.3.1] - 04-05-2023
### Fixed
- unnecessary headers in proxy requests ([dc94bb7](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/dc94bb7))

<a name="0.3.0"></a>
## [0.3.0] - 27-04-2023
### Added
- log level option to apps config and helm-chart ([d5e1987](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/d5e1987))

### Fixed
- the Dockerfile to install only main project dependencies ([ef95f57](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/ef95f57))

<a name="0.2.0"></a>
## [0.2.0] - 24-04-2023
### Added
- deployments resources settings to the helm-chart values ([9622506](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/9622506))
- name field to users and admin_users tables ([2451fcf](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/2451fcf))
- correlation_id to successful responses to the "id" field ([36e8500](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/36e8500))
- sqla pool configuration to helm-chart ([8e2884c](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/8e2884c))

### Fixed
- multiple connections usage in `v1_post_data` ([e132c37](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/e132c37))

<a name="0.1.3"></a>
## [0.1.3] - 04-04-2023
### Fixed
- multiple connections usage in `v1_post_data` ([617ab20](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/617ab20))

<a name="0.1.2"></a>
## [0.1.2] - 04-04-2023
### Added
- sqla pool configuration to helm-chart ([98e4106](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/98e4106))

<a name="0.1.0"></a>
## 0.1.0 - 03-04-2023
### Added
- integration tests for inner app ([0b5c587](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/0b5c587))
- integration tests for proxy aliases ([408b8ff](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/408b8ff))
- integration tests for logging of invalid requests ([23c720c](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/23c720c))
- logging of invalid requests to db and middleware logging ([bbd314d](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/bbd314d))
- integration tests for error codes ([582cd41](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/582cd41))
- error codes to exceptions responses ([c2ff9af](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/c2ff9af))
- integration tests for POST data endpoint ([f2aa530](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/f2aa530))
- integration tests and other changes: - Integration tests for auth in ingest_app and proxy_app - Integration tests for feed endpoint of ingest_app and proxy_app - More concise names of applications - Add Dockerfile - The public_proxy_feed route now proxies requests directly to ingest_app ([6512a1b](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/6512a1b))
- implementation of inner app and others: - Add custom CallbackStreamingResponse - Use pydantic.BaseSettings for settings delivery - Adapt global HTTPException handler to schemas.Error format - Add the "v_prepared_data" view from which data is exported to csv ([0288945](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/0288945))
- implementation of ingest_api and bc_proxy routers ([8317597](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/8317597))
- orm models, migrations and others changes ([faad843](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/faad843))
- project initial structure - Generate FastAPI schema using datamodel-code-generator - Add script for schemas generation - Append predefined openapi.json to FastAPI server - Stubs for routes - Stubs for exception handlers - Stubs for authorization - Minor openapi spec fixes ([4d0a448](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/4d0a448))
- openapi spec ([a2bc03f](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/a2bc03f))

### Changed
- Dockerfile to work with the nexus-proxy repository instead of pypi.org ([c4f1469](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/c4f1469))
- openapi spec files names ([97e935d](https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/commit/97e935d))


[0.3.2]: https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/compare/0.3.1...0.3.2
[0.3.1]: https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/compare/0.3.0...0.3.1
[0.3.0]: https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/compare/0.2.0...0.3.0
[0.2.0]: https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/compare/0.1.3...0.2.0
[0.1.3]: https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/compare/0.1.2...0.1.3
[0.1.2]: https://gitlab.com/scalhive.com/software-and-applications/data-management-platform/ingest-api/compare/0.1.1...0.1.2
