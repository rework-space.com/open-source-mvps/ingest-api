FROM python:3.11

WORKDIR /code
COPY ./ingest_api/ /code/ingest_api/
COPY ./docs/ /code/docs/

ENV NEXUS_PROXY_REPO=https://nexus-rm.dmp-insight.com/repository/pypi-proxy/simple

RUN pip install poetry==1.5.1 --index=$NEXUS_PROXY_REPO --no-cache-dir

COPY ./poetry.lock /code/poetry.lock
COPY ./pyproject.toml /code/pyproject.toml
RUN poetry source add --default proxy-pypi $NEXUS_PROXY_REPO
RUN poetry install --no-root --only main

ENTRYPOINT  ["poetry", "run", "uvicorn", "--app-dir=ingest_api", "--factory"]
