import typing

from starlette.exceptions import HTTPException as BaseHTTPException


class HTTPException(BaseHTTPException):

    def __init__(
            self,
            status_code: int,
            detail: typing.Optional[str] = None,
            error_code: int = None,
            headers: typing.Optional[dict] = None,
    ) -> None:
        self.error_code = error_code
        super().__init__(status_code, detail, headers)


class Unauthorized401(HTTPException):
    def __init__(self,
                 error_code: int,
                 headers: typing.Optional[dict] = None,
                 ):
        super().__init__(error_code=error_code,
                         status_code=401,
                         detail='Authentication information is missing '
                                'or invalid',
                         headers=headers)


class HTTPExceptions(Exception):

    def __init__(
            self,
            exceptions: list[HTTPException],
            headers: dict | None = None,
    ) -> None:
        self.exceptions = exceptions
        self.headers = headers

    def __repr__(self) -> str:
        class_name = self.__class__.__name__
        return f"{class_name}({self.exceptions})"


class EmptyListForbidden(ValueError):
    pass


class ErrorWrapper:
    pass
