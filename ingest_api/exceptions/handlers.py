from asgi_correlation_id import correlation_id
from fastapi import FastAPI, Request
from fastapi.exceptions import RequestValidationError
from fastapi.responses import JSONResponse
from pydantic.error_wrappers import _display_error_loc, \
    _display_error_type_and_ctx, ValidationError
from starlette import status
from starlette.exceptions import HTTPException as BaseStarletteHttpException

from .error_codes import pydantic_type_to_error_code, ErrorCodes
from .exceptions import HTTPException, HTTPExceptions


async def base_http_exception_handler(request: Request,
                                      exc: HTTPException):
    return JSONResponse(
        status_code=exc.status_code,
        content={
            "errors": [
                {
                    "id": correlation_id.get(),
                    "code": 0,
                    "message": exc.detail
                },
            ]
        },
        headers=exc.headers
    )


async def http_exception_handler(request: Request,
                                 exc: HTTPException):
    return JSONResponse(
        status_code=exc.status_code,
        content={
            "errors": [
                {
                    "id": correlation_id.get(),
                    "code": exc.error_code,
                    "message": exc.detail
                },
            ]
        },
        headers=exc.headers
    )


def make_http_exception_handler(code, message):
    async def http_exception_handler(request: Request,
                                     exc: HTTPException):
        return JSONResponse(
            status_code=exc.status_code,
            content={
                "errors": [
                    {
                        "id": correlation_id.get(),
                        "code": code,
                        "message": message
                    },
                ]
            },
            headers=exc.headers
        )

    return http_exception_handler


async def http_exceptions_handler(request: Request,
                                  excs: HTTPExceptions):

    return JSONResponse(content={
        "errors": [
            {
                "id": correlation_id.get(),
                "code": error.status_code,
                "message": error.detail
            } for error in excs.exceptions
        ]
    },
    )


async def validation_exception_handler(request: Request,
                                       excs: RequestValidationError,
                                       ):
    return JSONResponse(content={
        "errors": [
            {
                "id": correlation_id.get(),
                "code": pydantic_type_to_error_code(error['type']),
                "message": f'Loc: {_display_error_loc(error)}. Error: '
                           f'{error["msg"]} '
                           f'({_display_error_type_and_ctx(error)})'
            } for error in excs.errors()
        ]
    },
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY)


def register_exceptions_handlers(app: FastAPI):
    app.exception_handler(BaseStarletteHttpException)(
        base_http_exception_handler)
    app.exception_handler(HTTPException)(http_exception_handler)
    app.exception_handler(HTTPExceptions)(http_exceptions_handler)
    app.exception_handler(RequestValidationError)(
        validation_exception_handler)
    app.exception_handler(ValidationError)(validation_exception_handler)
    app.exception_handler(404)(make_http_exception_handler(
        code=ErrorCodes.not_found, message='Not found'))
    app.exception_handler(405)(make_http_exception_handler(
        code=ErrorCodes.method_not_allowed, message='Method not allowed'))
