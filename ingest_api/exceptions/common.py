from .exceptions import HTTPException
from .exceptions import Unauthorized401


def raise_401_api_key_not_provided():
    raise Unauthorized401(error_code=1)


def raise_401_user_not_found():
    raise Unauthorized401(error_code=2)


def raise_401_user_not_active():
    raise Unauthorized401(error_code=3)


def raise_404():
    raise HTTPException(status_code=404)
