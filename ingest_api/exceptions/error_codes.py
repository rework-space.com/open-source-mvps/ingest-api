from enum import IntEnum


class ErrorCodes(IntEnum):
    key_not_provided = 1
    user_not_found = 2
    user_not_active = 3
    not_found = 4
    method_not_allowed = 5
    missing_required_value = 6
    extra_values_forbidden = 7
    invalid_json = 8
    empty_list_forbidden = 9
    type_error = 10
    internal_server_error = 11
    unknown_error = 0


__type_to_error_code = {
    'value_error': {
        'jsondecode': ErrorCodes.invalid_json,
        'missing': ErrorCodes.missing_required_value,
        'extra': ErrorCodes.extra_values_forbidden,
        'emptylistforbidden': ErrorCodes.empty_list_forbidden,
    },
    'type_error': ErrorCodes.type_error,
}


def pydantic_type_to_error_code(type_):
    types = type_.split('.')

    if not types:
        return None

    code = __type_to_error_code
    for sub_type in types:
        code = code.get(sub_type)
        if isinstance(code, ErrorCodes):
            break
    if not isinstance(code, ErrorCodes):
        code = None
    if code is None:
        return ErrorCodes.unknown_error
    return code
