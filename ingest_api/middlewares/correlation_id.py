from dataclasses import dataclass, field
from typing import Callable
from uuid import uuid4

from asgi_correlation_id import \
    CorrelationIdMiddleware as BaseCorrelationIdMiddleware, correlation_id
from starlette.datastructures import MutableHeaders


@dataclass
class CorrelationIdMiddleware(BaseCorrelationIdMiddleware):
    generator: Callable[[], str] = field(default=lambda: str(uuid4()))

    async def __call__(self, scope, receive, send) -> None:
        """
        Load request ID from headers if present. Generate one otherwise.
        """
        if scope['type'] != 'http':
            await self.app(scope, receive, send)
            return

        # Try to load request ID from the request headers
        headers = MutableHeaders(scope=scope)
        header_value = headers.get(self.header_name.lower())

        if not header_value:
            # Generate request ID if none was found
            id_value = self.generator()
        elif self.validator and not self.validator(header_value):
            # Also generate a request ID if one was found, but it was deemed
            # invalid
            id_value = self.generator()
        else:
            # Otherwise, use the found request ID
            id_value = header_value

        # Clean/change the ID if needed
        if self.transformer:
            id_value = self.transformer(id_value)

        # Update the request headers if needed
        if id_value != header_value and self.update_request_header is True:
            headers[self.header_name] = id_value

        correlation_id.set(id_value)
        self.sentry_extension(id_value)

        async def handle_outgoing_request(message) -> None:
            if message['type'] == 'http.response.start' \
                    and correlation_id.get():
                headers = MutableHeaders(scope=message)
                if self.header_name not in headers:
                    headers.append(self.header_name, correlation_id.get())

            await send(message)

        await self.app(scope, receive, handle_outgoing_request)
        return
