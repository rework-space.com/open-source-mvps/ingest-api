from dataclasses import dataclass
from pathlib import Path

from pydantic import BaseSettings, Field
from pydantic.env_settings import SettingsSourceCallable


@dataclass
class Paths:
    root_path: Path = Path(__file__).parent.parent.resolve()
    ingest_openapi_spec: Path = root_path / 'docs' / 'ingest-openapi.json'
    inner_openapi_spec: Path = root_path / 'docs' / 'admin-openapi.json'
    pyproject_file: Path = root_path / 'pyproject.toml'


class BaseIngestAPISettings(BaseSettings):
    paths: Paths = Field(default_factory=Paths)

    log_level: str = Field(default='INFO', env='LOG_LEVEL')
    # the number of connections to allow in connection pool “overflow”
    pool_max_overflow: int = Field(default=10, env='POOL_MAX_OVERFLOW')
    # the number of connections to keep open inside the connection pool
    pool_size: int = Field(default=5, env='POOL_SIZE')
    # number of seconds to wait before giving up on getting a connection
    # from the pool
    pool_timeout: int = Field(default=30, env='POOL_TIMEOUT')
    # this setting causes the pool to recycle connections after the given
    # number of seconds has passed.
    pool_recycle: int = Field(default=-1, env='POOL_RECYCLE')
    # if True will enable the connection pool “pre-ping” feature that tests
    # connections for liveness upon each checkout  Task actions
    pool_pre_ping: bool = Field(default=True, env='POOL_PRE_PING')

    class Config:
        @classmethod
        def customise_sources(
            cls,
            init_settings: SettingsSourceCallable,
            env_settings: SettingsSourceCallable,
            file_secret_settings: SettingsSourceCallable,
        ) -> tuple[SettingsSourceCallable, ...]:
            return env_settings, init_settings


class IngestAppSettings(BaseIngestAPISettings):
    ingest_postgres_conn_url: str = Field(env='INGEST_POSTGRES_CONN_URL')


class ProxyAppSettings(BaseIngestAPISettings):
    proxy_postgres_conn_url: str = Field(env='PROXY_POSTGRES_CONN_URL')
    ingest_app_feed_uri: str = Field(env='INGEST_APP_FEED_URI')


class AdminAppSettings(BaseIngestAPISettings):
    admin_postgres_conn_url: str = Field(env='ADMIN_POSTGRES_CONN_URL')


class AlembicSettings(BaseIngestAPISettings):
    admin_postgres_conn_url: str = Field(env='ADMIN_POSTGRES_CONN_URL')
