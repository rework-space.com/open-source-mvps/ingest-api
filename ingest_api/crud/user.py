from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import contains_eager, joinedload

from models import (User, AliasesConfig, ProxyConfig, UserConfig,
                    PublicEndpointConfig, AdminUser)


async def get_user_by_api_key(db: AsyncSession,
                              api_key: str) -> User | None:
    return (await db.execute(select(
        User
    ).where(
        User.api_key == api_key,
    ))).scalars().one_or_none()


async def get_admin_user_by_api_key(db: AsyncSession,
                                    api_key: str) -> AdminUser | None:
    return (await db.execute(select(
        AdminUser
    ).where(
        AdminUser.api_key == api_key,
    ))).scalars().one_or_none()


async def get_user_with_aliases_config_by_api_key(
        db: AsyncSession,
        api_key: str
) -> User | None:
    return (await db.execute(select(
        User
    ).join(
        UserConfig
    ).join(
        ProxyConfig
    ).join(
        AliasesConfig
    ).filter(
        User.api_key == api_key,
    ).options(
        contains_eager(User.config).defer('*').options(
            contains_eager(
                UserConfig.proxy_config
            ).defer('*').options(
                contains_eager(
                    ProxyConfig.aliases_config
                )
            ),
        ),
    ))).scalars().one_or_none()


async def get_user_with_full_proxy_config_by_public_path(
        db: AsyncSession,
        public_path: str
) -> User | None:
    return (await db.execute(select(
        User
    ).join(
        UserConfig
    ).join(
        ProxyConfig
    ).join(
        PublicEndpointConfig
    ).filter(
        PublicEndpointConfig.path_parameter == public_path,
    ).options(
        contains_eager(User.config).defer('*').options(
            contains_eager(UserConfig.proxy_config).defer('*').options(
                contains_eager(ProxyConfig.public_endpoint_config),
                joinedload(ProxyConfig.aliases_config)
            ),
        ),
    ))).scalars().one_or_none()
