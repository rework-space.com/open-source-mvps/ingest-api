import io
from datetime import datetime
from typing import Callable, Awaitable
from uuid import UUID

import pandas as pd
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncEngine, AsyncSession

import models
import schemas
from models import VPreparedData


async def push_data_to_db(db: AsyncSession,
                          data: schemas.DataBatch,
                          user: models.User):
    data = data.dict()['__root__']
    table = models.DataRow.__tablename__
    schema = models.DataRow.__table_args__['schema']

    df = pd.DataFrame(data, dtype=object)
    df['_user_id'] = user.id
    buffer = io.BytesIO()
    df.to_csv(buffer, index=False, header=True)
    buffer.seek(0)

    conn = await db.connection()
    raw_conn = await conn.get_raw_connection()
    await raw_conn.driver_connection.copy_to_table(
        table_name=table, schema_name=schema, source=buffer,
        columns=tuple(df.columns), header=True, format='csv',
        delimiter=','
    )
    await conn.commit()


async def push_data_row_to_db(db: AsyncSession,
                              data_row: schemas.DataRow,
                              user: models.User):
    data_row_model = models.DataRow(**data_row.dict())
    data_row_model.user = user
    db.add(data_row_model)
    await db.commit()


async def copy_data_from_db_to_file(
        engine: AsyncEngine, start_date: datetime, end_date: datetime,
        data_asset_id: UUID, output: Callable[[bytes], Awaitable[None]]):
    start_date, end_date = (int(start_date.timestamp()),
                            int(end_date.timestamp()))
    q = select(
        VPreparedData
    ).filter(
        VPreparedData.data_asset_id == data_asset_id,
        VPreparedData.created_at >= start_date,
        VPreparedData.created_at < end_date
    ).compile(engine)

    async with engine.connect() as conn:
        raw_conn = await conn.get_raw_connection()
        await raw_conn.driver_connection.copy_from_query(
            str(q), data_asset_id, start_date, end_date, output=output,
            format='csv', header=True
        )
