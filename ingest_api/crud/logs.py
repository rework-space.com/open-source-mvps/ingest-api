from fastapi import Request
from sqlalchemy.ext.asyncio import AsyncSession

import models


async def log_invalid_request_to_db(db: AsyncSession,
                                    request: Request,
                                    user: models.User,
                                    request_id: str,
                                    error: Exception | str):
    invalid_request = models.InvalidRequest(
        user_id=user.id,
        client=f'{request.client.host}:{request.client.port}',
        request_id=request_id,
        method=request.method,
        path=str(request.url),
        query_params=str(request.query_params),
        body=str(await request.body()),
        error=str(error),
    )

    db.add(invalid_request)
    await db.commit()
