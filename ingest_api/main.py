from apps import create_proxy_app, create_ingest_app, \
    create_admin_app


def ingest_api_and_proxy_app():
    main_app = create_ingest_app()
    proxy_app = create_proxy_app()
    main_app.mount('/bc-proxy', proxy_app)
    return main_app
