from typing import Optional, Callable, Mapping, Awaitable

from starlette.background import BackgroundTask
from starlette.responses import StreamingResponse
from starlette.types import Send

SendBytes = Callable[[bytes], Awaitable[None]]


class CallBackStreamingResponse(StreamingResponse):

    def __init__(
            self,
            func: Callable[[SendBytes], Awaitable[None]],
            status_code: int = 200,
            headers: Optional[Mapping[str, str]] = None,
            media_type: Optional[str] = None,
            background: Optional[BackgroundTask] = None,
    ) -> None:
        self.func = func
        self.status_code = status_code
        self.media_type = self.media_type if media_type is None else media_type
        self.background = background
        self.init_headers(headers)

    async def stream_response(self, send: Send) -> None:
        await send(
            {
                "type": "http.response.start",
                "status": self.status_code,
                "headers": self.raw_headers,
            }
        )

        async def _send(b):
            await send({"type": "http.response.body", "body": bytes(b),
                        "more_body": True})

        await self.func(_send)
        await send(
            {"type": "http.response.body", "body": b"", "more_body": False})
