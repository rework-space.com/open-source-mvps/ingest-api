from typing import Mapping


def renames_query_params(query_params: Mapping, naming_dict: Mapping):
    return {naming_dict.get(k): v for k, v in query_params.items()}
