from sqlalchemy_wrapper import SQLAlchemy


class FastAPIDBManager:

    def __init__(self):
        self._dbs: dict[str, SQLAlchemy] = {}

    def add(self, key: str, db: SQLAlchemy):
        cur_db = self._dbs.get(key)
        if cur_db is None:
            self._dbs[key] = db

    def get(self, key: str) -> SQLAlchemy:
        db = self._dbs.get(key)
        if db is None:
            raise ValueError(f"Database {key!r} not exists")
        return db

    def get_session(self, key):
        db = self.get(key)
        return db.Session()

    def get_engine(self, key):
        db = self.get(key)
        return db.engine
