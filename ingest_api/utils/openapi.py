import json
from pathlib import Path


def load_json_openapi(filepath: str | Path) -> dict:
    with open(filepath) as file:
        return json.load(file)
