from aiohttp import ClientSession


__session = ClientSession()


async def get_aioclient():
    return __session


__all__ = ['get_aioclient']
