from fastapi import Depends
from fastapi import Path
from fastapi.security import APIKeyHeader
from sqlalchemy.ext.asyncio import AsyncSession

from crud.user import (get_user_by_api_key,
                       get_user_with_aliases_config_by_api_key,
                       get_admin_user_by_api_key,
                       get_user_with_full_proxy_config_by_public_path)
from exceptions import (raise_401_api_key_not_provided,
                        raise_401_user_not_found,
                        raise_401_user_not_active)
from exceptions import raise_404
from models import User
from .database import get_db

api_key_query = APIKeyHeader(name='X-API-KEY', auto_error=False)


async def auth_user(api_key: str | None = Depends(api_key_query),
                    db: AsyncSession = Depends(get_db('ingest_db'))) -> User:
    if not api_key:
        raise_401_api_key_not_provided()

    user = await get_user_by_api_key(db, api_key)
    if user is None:
        raise_401_user_not_found()
    if not user.enable:
        raise_401_user_not_active()
    return user


async def auth_admin_user(api_key: str | None = Depends(api_key_query),
                          db: AsyncSession = Depends(get_db('admin_db'))
                          ) -> User:
    if not api_key:
        raise_401_api_key_not_provided()

    user = await get_admin_user_by_api_key(db, api_key)
    if user is None:
        raise_401_user_not_found()
    if not user.enable:
        raise_401_user_not_active()
    return user


async def auth_user_with_aliases_config(
        api_key: str | None = Depends(api_key_query),
        db: AsyncSession = Depends(get_db('proxy_db'))
) -> User:
    """Additionally loads user aliases_config."""
    if not api_key:
        raise_401_api_key_not_provided()

    user = await get_user_with_aliases_config_by_api_key(db, api_key)
    if not user:
        raise_401_user_not_found()
    if not user.enable:
        raise_401_user_not_active()
    return user


async def auth_user_by_provider_path(
        provider: str = Path(),
        db: AsyncSession = Depends(get_db('proxy_db')),
) -> User:
    if not provider:
        raise_404()

    user = await get_user_with_full_proxy_config_by_public_path(db, provider)
    if not user:
        raise_404()
    if not user.enable:
        raise_404()
    if not user.config.proxy_config.public_endpoint_config.enable:
        raise_404()
    return user
