from typing import Mapping

from pydantic.tools import lru_cache


class Constants(Mapping):

    def __init__(self):
        self.__dict = {}

    def __getitem__(self, key):
        return self.__dict.__getitem__(key)

    def __len__(self) -> int:
        return self.__dict.__len__()

    def __iter__(self):
        return self.__dict.__iter__()

    def __setitem__(self, key, value):
        if self.__dict.get(key):
            raise ValueError(f'Constant {key} already exists')
        self.__dict.__setitem__(key, value)

    def set(self, key, value):
        self[key] = value

    def get(self, key):
        return self[key]


__constants = Constants()


@lru_cache
def get_const(name):
    def get_const_deps():
        return __constants.get(name)
    return get_const_deps


def set_consts(**kwargs):
    for name, value in kwargs.items():
        __constants.set(name, value)


__all__ = ['get_const', 'set_consts']
