from functools import lru_cache

from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy_wrapper import SQLAlchemy

from utils.db_manager import FastAPIDBManager
from fastapi import Request

db_manager = FastAPIDBManager()


@lru_cache
def get_db_engine(name):
    async def get_db_engine_deps():
        engine = db_manager.get_engine(name)
        yield engine

    return get_db_engine_deps


@lru_cache
def get_db(name):
    async def get_db_deps(request: Request):
        session = db_manager.get_session(name)
        try:
            yield session
        finally:
            await session.close()

    return get_db_deps


def _create_db(postgres_conn_url: str, engine_options: dict | None = None,
               session_options: dict | None = None) -> SQLAlchemy:
    if engine_options is None:
        engine_options = {}
    engine_options = {"type_": 'async',
                      "echo": False,
                      **engine_options}
    if session_options is None:
        session_options = {}
    session_options = {'class_': AsyncSession,
                       'expire_on_commit': False,
                       **session_options}
    return SQLAlchemy(postgres_conn_url, engine_options=engine_options,
                      session_options=session_options)


def set_db(name, postgres_conn_url, engine_options: dict | None = None,
           session_options: dict | None = None):
    db = _create_db(postgres_conn_url, engine_options, session_options)
    db_manager.add(name, db)


__all__ = ['set_db', 'get_db', 'get_db_engine']
