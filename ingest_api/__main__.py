import sys
from pathlib import Path


sys.path.insert(0, str(Path(__file__).parent.resolve()))


from models import registry
from sqlalchemy_wrapper import Alembic, SQLAlchemy
from config import AlembicSettings


config = AlembicSettings()
alembic = Alembic(db=SQLAlchemy(config.admin_postgres_conn_url),
                  registry=registry,
                  )
alembic.get_click_cli('db')()
