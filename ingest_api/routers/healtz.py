from fastapi import FastAPI, Request, Response


async def healtz(request: Request):
    return Response(status_code=200,
                    content='OK')


def register_healtz_routers(app: FastAPI):
    app.add_route('/healtz', healtz)
