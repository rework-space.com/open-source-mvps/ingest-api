from fastapi import FastAPI, Request
from starlette.responses import RedirectResponse


def root(request: Request):
    return RedirectResponse('/docs')


def register_root_routers(app: FastAPI):
    app.add_route('/', root)
    app.post('/data', status_code=201)
