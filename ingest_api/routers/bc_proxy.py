"""Backward Compatibility Proxy routes"""
import aiohttp
from asgi_correlation_id import correlation_id
from fastapi import FastAPI, APIRouter, Request, Depends
from sqlalchemy.ext.asyncio import AsyncSession
from starlette.responses import Response

from crud.logs import log_invalid_request_to_db
from dependencies.aioclient import get_aioclient
from dependencies.constants import get_const
from dependencies.database import get_db
from dependencies.security import (auth_user_with_aliases_config,
                                   auth_user_by_provider_path, api_key_query)
from models import User, AliasesConfig


def rename_query_params(query_params, aliases_config: AliasesConfig):
    pymodel = aliases_config.to_pydantic()
    pymodel.validate(query_params)
    naming_dict = aliases_config.reversed_dict()
    return {naming_dict.get(k): v for k, v in query_params.items()}


async def auth_proxy_feed(
        request: Request,
        db: AsyncSession = Depends(get_db('proxy_db')),
        user: User = Depends(auth_user_with_aliases_config),
        api_key: str = Depends(api_key_query),
        aioclient: aiohttp.ClientSession = Depends(get_aioclient),
        ingest_api_uri: str = Depends(get_const('ingest_app_feed_uri'))
):
    try:
        headers = {api_key_query.model.name: api_key}

        renamed_query_params = rename_query_params(
            request.query_params, user.config.proxy_config.aliases_config)
        async with aioclient.get(ingest_api_uri, params=renamed_query_params,
                                 headers=headers) as resp:
            if not resp.ok:
                await log_invalid_request_to_db(
                    db=db, request=request, user=user,
                    request_id=correlation_id.get(),
                    error=f'proxy_{resp.status}')
            return Response(content=await resp.read(),
                            status_code=resp.status,
                            headers=resp.headers,
                            media_type=resp.content_type)
    except Exception as exc:
        await log_invalid_request_to_db(db=db, request=request, user=user,
                                        request_id=correlation_id.get(),
                                        error=exc)
        raise exc


async def public_proxy_feed(
        request: Request,
        db: AsyncSession = Depends(get_db('proxy_db')),
        user: User = Depends(auth_user_by_provider_path),
        aioclient: aiohttp.ClientSession = Depends(get_aioclient),
        ingest_api_uri: str = Depends(get_const('ingest_app_feed_uri'))
):
    try:
        headers = {api_key_query.model.name: user.api_key}

        renamed_query_params = rename_query_params(
            request.query_params, user.config.proxy_config.aliases_config)

        async with aioclient.get(ingest_api_uri, params=renamed_query_params,
                                 headers=headers) as resp:
            if not resp.ok:
                await log_invalid_request_to_db(
                    db=db, request=request, user=user,
                    request_id=correlation_id.get(),
                    error=f'proxy_{resp.status}')
            return Response(content=await resp.read(),
                            status_code=resp.status,
                            headers=resp.headers,
                            media_type=resp.content_type)
    except Exception as exc:
        await log_invalid_request_to_db(db=db, request=request, user=user,
                                        request_id=correlation_id.get(),
                                        error=exc)
        raise exc


def register_v1_routers(app: FastAPI):
    router = APIRouter()
    router.get("/", status_code=201)(auth_proxy_feed)
    router.get("/{provider}", status_code=201)(public_proxy_feed)
    app.include_router(router)
