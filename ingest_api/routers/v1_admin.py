from datetime import datetime
from uuid import UUID

from fastapi import APIRouter, FastAPI, Depends, Query
from sqlalchemy.ext.asyncio import AsyncEngine

from crud.data import copy_data_from_db_to_file
from dependencies.database import get_db_engine
from dependencies.security import auth_admin_user
from utils.responses import CallBackStreamingResponse


async def v1_data_batch_download(
        start_date: datetime = Query(),  # ISO 8601
        end_date: datetime = Query(),  # ISO 8601
        data_asset_id: UUID = Query(),
        db_engine: AsyncEngine = Depends(get_db_engine('admin_db')),
):
    async def stream(send):
        await copy_data_from_db_to_file(
            engine=db_engine, start_date=start_date, end_date=end_date,
            data_asset_id=data_asset_id, output=send)
    return CallBackStreamingResponse(stream, status_code=200,
                                     media_type='text/csv')


def register_v1_inner_routers(app: FastAPI):
    router = APIRouter()
    router.get("/api/v1/data",
               dependencies=[Depends(auth_admin_user)],
               status_code=200)(v1_data_batch_download)
    app.include_router(router)
