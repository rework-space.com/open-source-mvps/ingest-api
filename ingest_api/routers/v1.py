import json

import pydantic
from asgi_correlation_id import correlation_id
from fastapi import APIRouter, Request, FastAPI, Depends
from fastapi.exceptions import RequestValidationError
from pydantic.error_wrappers import ErrorWrapper
from sqlalchemy.ext.asyncio import AsyncSession

import models
import schemas
from crud.data import push_data_to_db, push_data_row_to_db
from crud.logs import log_invalid_request_to_db
from dependencies.database import get_db
from dependencies.security import auth_user


async def v1_get_data(
        request: Request,
        user: models.User = Depends(auth_user),
        db_main: AsyncSession = Depends(get_db('ingest_db'))
) -> schemas.ApiV1DataFeedGetResponse:
    """Push data in query parameters."""
    try:
        data_row = schemas.DataRow.validate(request.query_params)
        await push_data_row_to_db(db_main, data_row, user)
        return schemas.ApiV1DataFeedGetResponse(
            data=schemas.Data(id=correlation_id.get()))
    except Exception as e:
        await log_invalid_request_to_db(db_main, request, user,
                                        correlation_id.get(), e)
        raise e


async def v1_post_data(
        request: Request,
        user: models.User = Depends(auth_user),
        db_main: AsyncSession = Depends(get_db('ingest_db'))
) -> schemas.ApiV1DataPostResponse:
    """Push Data in json format. Email column are required.
    The another columns are optionals"""
    try:
        post_request = schemas.ApiV1DataPostRequest.validate(
            await request.json()
        )
        await push_data_to_db(db_main, post_request.data, user)
        return schemas.ApiV1DataPostResponse(
            data=schemas.Data(id=correlation_id.get()))
    except json.JSONDecodeError as e:
        await log_invalid_request_to_db(db_main, request, user,
                                        correlation_id.get(), e)
        e.doc = None  # don't send body request body
        raise RequestValidationError([ErrorWrapper(e, ("body", e.pos))]) from e
    except pydantic.errors.DictError as e:
        await log_invalid_request_to_db(db_main, request, user,
                                        correlation_id.get(), e)
        raise RequestValidationError([ErrorWrapper(e, ("body", 0))]) from e
    except Exception as e:
        await log_invalid_request_to_db(db_main, request, user,
                                        correlation_id.get(), e)
        raise


def register_v1_routers(app: FastAPI):
    router = APIRouter()
    router.get("/api/v1/data/feed", status_code=201)(v1_get_data)
    router.post("/api/v1/data", status_code=201)(v1_post_data)
    app.include_router(router)
