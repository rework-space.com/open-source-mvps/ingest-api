from sqlalchemy import select, cast, extract, Integer

from ..schema import __schema_name__
from ...base import registry
from ..tables import User, DataRow


class VPreparedData(registry.View):
    __view_name__ = 'v_prepared_data'  # The name of the view to create
    __schema__ = __schema_name__  # The name of the schema
    # An SQLAlchemy selectable e.g. a select() statement
    __definition__ = select(
        DataRow._id.label('row_id'),
        DataRow.country.label('country'),
        DataRow.email.label('email'),
        DataRow.email_hash_md5_content.label('email_hash_md5_content'),
        DataRow.firstname.label('firstname'),
        DataRow.lastname.label('lastname'),
        DataRow.ip.label('ip'),
        DataRow.url.label('url'),
        DataRow.date.label('date'),
        DataRow.dob.label('dob'),
        DataRow.gender.label('gender'),
        DataRow.address1.label('address1'),
        DataRow.address2.label('address2'),
        DataRow.zip.label('zip'),
        DataRow.city.label('city'),
        DataRow.state.label('state'),
        DataRow.vertical.label('vertical'),
        DataRow.language.label('language'),
        DataRow.phone_number1.label('phone_number1'),
        DataRow.phone_number2.label('phone_number2'),
        DataRow.age.label('age'),
        cast(extract('EPOCH', DataRow._created_at),
             Integer).label('created_at'),
        User.data_asset_id.label('data_asset_id'),
    ).join(User)
