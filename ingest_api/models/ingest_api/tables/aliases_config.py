from typing import Type

from pydantic import create_model, Extra, BaseModel
from sqlalchemy import Column, UUID, text, DateTime, TEXT

from ..schema import __schema_name__
from ...base import registry


class AliasesConfig(registry.Table):
    __tablename__ = 'aliases_configs'
    __table_args__ = (
        {'schema': __schema_name__}
    )

    _id = Column(UUID, primary_key=True,
                 server_default=text("uuid_generate_v4()"))
    country = Column(TEXT, default='country', server_default='country',
                     nullable=False)
    email = Column(TEXT, default='email', server_default='email',
                   nullable=False)
    email_hash_md5_content = Column(TEXT, default='email_hash_md5_content',
                                    server_default='email_hash_md5_content',
                                    nullable=False)
    firstname = Column(TEXT, default='firstname', server_default='firstname',
                       nullable=False)
    lastname = Column(TEXT, default='lastname', server_default='lastname',
                      nullable=False)
    ip = Column(TEXT, default='ip', server_default='ip', nullable=False)
    url = Column(TEXT, default='url', server_default='url', nullable=False)
    date = Column(TEXT, default='date', server_default='date', nullable=False)
    dob = Column(TEXT, default='dob', server_default='dob', nullable=False)
    gender = Column(TEXT, default='gender', server_default='gender',
                    nullable=False)
    address1 = Column(TEXT, default='address1', server_default='address1',
                      nullable=False)
    address2 = Column(TEXT, default='address2', server_default='address2',
                      nullable=False)
    zip = Column(TEXT, default='zip', server_default='zip', nullable=False)
    city = Column(TEXT, default='city', server_default='city', nullable=False)
    state = Column(TEXT, default='state', server_default='state',
                   nullable=False)
    vertical = Column(TEXT, default='vertical', server_default='vertical',
                      nullable=False)
    language = Column(TEXT, default='language', server_default='language',
                      nullable=False)
    phone_number1 = Column(TEXT, default='phone_number1',
                           server_default='phone_number1', nullable=False)
    phone_number2 = Column(TEXT, default='phone_number2',
                           server_default='phone_number2', nullable=False)
    age = Column(TEXT, default='age', server_default='age', nullable=False)
    _created_at = Column(DateTime(timezone=True), nullable=False,
                         server_default=text("now()"))

    def reversed_dict(self):
        return {v: k for k, v in self.__dict__.items()
                if not k.startswith('_')}

    @property
    def aliases_columns(self):
        return (v for k, v in self.__dict__.items() if not k.startswith('_'))

    @property
    def required_columns(self):
        return self.__dict__['email'],

    def to_pydantic(self) -> Type[BaseModel]:
        # TODO: optimize pydantic model creation
        class Config:
            extra = Extra.forbid

        aliases = {column: (str, None) for column in self.aliases_columns}
        for column in self.required_columns:
            aliases[column] = (str, ...)
        return create_model(
            'AliasSchemaModel',
            **aliases,
            __config__=Config
        )
