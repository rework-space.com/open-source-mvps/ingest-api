from sqlalchemy import Column, UUID, text, ForeignKey, TEXT, DateTime
from sqlalchemy.orm import relationship

from ...base import registry
from ..schema import __schema_name__
from .user import User


class InvalidRequest(registry.Table):
    __tablename__ = 'invalid_requests'
    __table_args__ = (
        {'schema': __schema_name__}
    )

    id = Column(UUID, primary_key=True,
                server_default=text("uuid_generate_v4()"))
    user_id = Column(ForeignKey(User.id, ondelete="RESTRICT",
                                onupdate="RESTRICT"), nullable=True)
    request_id = Column(UUID, nullable=False)
    client = Column(TEXT, nullable=False)
    method = Column(TEXT, nullable=False)
    path = Column(TEXT, nullable=False)
    query_params = Column(TEXT, nullable=True)
    body = Column(TEXT, nullable=True)
    error = Column(TEXT, nullable=True)
    created_at = Column(DateTime(timezone=True), nullable=False,
                        server_default=text("now()"))

    user = relationship(User, backref='invalid_requests')
