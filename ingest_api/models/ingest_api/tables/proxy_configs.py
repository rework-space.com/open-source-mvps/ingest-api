from sqlalchemy import Column, UUID, text, DateTime, String, ForeignKey
from sqlalchemy.orm import relationship

from .aliases_config import AliasesConfig
from .public_endpoint_config import PublicEndpointConfig
from ..schema import __schema_name__
from ...base import registry


class ProxyConfig(registry.Table):
    __tablename__ = 'proxy_configs'
    __table_args__ = (
        {'schema': __schema_name__}
    )

    id = Column(UUID, primary_key=True,
                server_default=text("uuid_generate_v4()"))
    name = Column(String)
    aliases_config_id = Column(ForeignKey(
        AliasesConfig._id, ondelete="RESTRICT",
        onupdate="RESTRICT"), nullable=False)
    public_endpoint_config_id = Column(ForeignKey(
        PublicEndpointConfig.id, ondelete="RESTRICT",
        onupdate="RESTRICT"), nullable=True, unique=True)

    created_at = Column(DateTime(timezone=True), nullable=False,
                        server_default=text("now()"))

    aliases_config = relationship(
        AliasesConfig, backref='proxy_config',
    )
    public_endpoint_config = relationship(
        PublicEndpointConfig, backref='proxy_config',
    )
