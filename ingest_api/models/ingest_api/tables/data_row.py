from sqlalchemy import Column, UUID, text, DateTime, TEXT, ForeignKey, Integer
from sqlalchemy.orm import relationship

from .user import User
from ..schema import __schema_name__
from ...base import registry


class DataRow(registry.Table):
    __tablename__ = 'data'
    __table_args__ = (
        {'schema': __schema_name__}
    )

    _id = Column(UUID, primary_key=True,
                 server_default=text("uuid_generate_v4()"))
    country = Column(TEXT, nullable=True)
    email = Column(TEXT, nullable=False)
    email_hash_md5_content = Column(TEXT, nullable=True)
    firstname = Column(TEXT, nullable=True)
    lastname = Column(TEXT, nullable=True)
    ip = Column(TEXT, nullable=True)
    url = Column(TEXT, nullable=True)
    date = Column(TEXT, nullable=True)
    dob = Column(TEXT, nullable=True)
    gender = Column(TEXT, nullable=True)
    address1 = Column(TEXT, nullable=True)
    address2 = Column(TEXT, nullable=True)
    zip = Column(TEXT, nullable=True)
    city = Column(TEXT, nullable=True)
    state = Column(TEXT, nullable=True)
    vertical = Column(TEXT, nullable=True)
    language = Column(TEXT, nullable=True)
    phone_number1 = Column(TEXT, nullable=True)
    phone_number2 = Column(TEXT, nullable=True)
    age = Column(Integer, nullable=True)

    _created_at = Column(DateTime(timezone=True), nullable=False,
                         server_default=text("now()"))
    _user_id = Column(ForeignKey(
        User.id, ondelete="RESTRICT", onupdate="RESTRICT"), nullable=True)
    user = relationship(User, backref='data')
