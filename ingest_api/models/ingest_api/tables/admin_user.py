from sqlalchemy import Column, UUID, text, Boolean, DateTime, TEXT

from ..domains import ApiKey
from ..schema import __schema_name__
from ...base import registry
from ..functions import create_api_key


class AdminUser(registry.Table):
    __tablename__ = 'admin_users'
    __table_args__ = (
        {'schema': __schema_name__}
    )

    id = Column(UUID, primary_key=True,
                server_default=text("uuid_generate_v4()"))
    name = Column(TEXT)
    enable = Column(Boolean, default=False, server_default='f',
                    nullable=False)
    api_key = Column(ApiKey, server_default=create_api_key(), nullable=False,
                     unique=True)
    created_at = Column(DateTime(timezone=True), nullable=False,
                        server_default=text("now()"))
