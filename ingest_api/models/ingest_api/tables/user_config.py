from sqlalchemy import Column, UUID, text, ForeignKey, DateTime
from sqlalchemy.orm import relationship

from .proxy_configs import ProxyConfig
from ..schema import __schema_name__
from ...base import registry


class UserConfig(registry.Table):
    __tablename__ = 'users_configs'
    __table_args__ = (
        {'schema': __schema_name__}
    )

    id = Column(UUID, primary_key=True,
                server_default=text("uuid_generate_v4()"))
    proxy_config_id = Column(ForeignKey(
        ProxyConfig.id, ondelete="RESTRICT",
        onupdate="RESTRICT"), nullable=True)
    created_at = Column(DateTime(timezone=True), nullable=False,
                        server_default=text("now()"))

    proxy_config = relationship(ProxyConfig,
                                backref='user_config')
