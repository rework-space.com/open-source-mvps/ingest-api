from sqlalchemy import Column, UUID, text, DateTime, String, \
    Boolean

from ..schema import __schema_name__
from ...base import registry


class PublicEndpointConfig(registry.Table):
    __tablename__ = 'public_endpoint_configs'
    __table_args__ = (
        {'schema': __schema_name__}
    )

    id = Column(UUID, primary_key=True,
                server_default=text("uuid_generate_v4()"))
    enable = Column(Boolean, default=False, server_default='f',
                    nullable=False)
    path_parameter = Column(String, nullable=False, unique=True)
    created_at = Column(DateTime(timezone=True), nullable=False,
                        server_default=text("now()"))
