from sqlalchemy import Column, UUID, text, ForeignKey, Boolean, DateTime, TEXT
from sqlalchemy.orm import relationship

from .user_config import UserConfig
from ..domains import ApiKey
from ..functions import create_api_key
from ..schema import __schema_name__
from ...base import registry


class User(registry.Table):
    __tablename__ = 'users'
    __table_args__ = (
        {'schema': __schema_name__}
    )

    id = Column(UUID, primary_key=True,
                server_default=text("uuid_generate_v4()"))
    name = Column(TEXT)
    data_asset_id = Column(UUID, nullable=False)
    user_config_id = Column(ForeignKey(
        UserConfig.id, ondelete="RESTRICT",
        onupdate="RESTRICT"), nullable=False)
    enable = Column(Boolean, default=False, server_default='f',
                    nullable=False)
    api_key = Column(ApiKey, server_default=create_api_key(), nullable=False,
                     unique=True)
    created_at = Column(DateTime(timezone=True), nullable=False,
                        server_default=text("now()"))

    config = relationship(UserConfig, backref='user')
