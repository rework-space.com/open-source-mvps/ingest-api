from sqlalchemy.sql.type_api import UserDefinedType


class ApiKey(UserDefinedType):

    cache_ok = True

    def get_col_spec(self, **kwargs):
        return 'ingest_api.api_key'
