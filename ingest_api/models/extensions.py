from .base import registry


class UUIDExtension(registry.Extension):
    __schema__ = 'public'
    __signature__ = 'uuid-ossp'
