from fastapi import FastAPI

import exceptions
from config import IngestAppSettings
from dependencies import database
from logger import setup_logging
from middlewares import CorrelationIdMiddleware, logging_middleware
from routers import root, healtz, v1
from utils.openapi import load_json_openapi


def register_routers(app: FastAPI):
    healtz.register_healtz_routers(app)
    root.register_root_routers(app)
    v1.register_v1_routers(app)


def register_exceptions_handlers(app: FastAPI):
    exceptions.register_exceptions_handlers(app)


def register_middlewares(app: FastAPI):
    app.middleware('http')(logging_middleware)
    app.add_middleware(CorrelationIdMiddleware)


def create_ingest_app(config: IngestAppSettings | None = None):
    if config is None:
        config = IngestAppSettings()

    app = FastAPI(title='Ingest API', redoc_url=None)
    setup_logging(log_level=config.log_level)

    app.openapi_schema = load_json_openapi(config.paths.ingest_openapi_spec)

    database.set_db('ingest_db', config.ingest_postgres_conn_url,
                    engine_options={
                        'pool_size': config.pool_size,
                        'max_overflow': config.pool_max_overflow,
                        'pool_pre_ping': config.pool_pre_ping,
                        'pool_recycle': config.pool_recycle,
                        'pool_timeout': config.pool_timeout,
                    })

    register_routers(app)
    register_exceptions_handlers(app)
    register_middlewares(app)
    return app
