from fastapi import FastAPI

import exceptions
from config import ProxyAppSettings
from dependencies import database, constants
from logger import setup_logging
from middlewares import logging_middleware, CorrelationIdMiddleware
from routers import healtz, bc_proxy


def register_routers(app: FastAPI):
    healtz.register_healtz_routers(app)
    bc_proxy.register_v1_routers(app)


def register_exceptions_handlers(app: FastAPI):
    exceptions.register_exceptions_handlers(app)


def register_middlewares(app: FastAPI):
    app.middleware('http')(logging_middleware)
    app.add_middleware(CorrelationIdMiddleware)


def create_proxy_app(config: ProxyAppSettings | None = None):
    if config is None:
        config = ProxyAppSettings()

    app = FastAPI(openapi_url=None, swagger_ui_oauth2_redirect_url=None,
                  docs_url=None,
                  redoc_url=None)
    setup_logging(log_level=config.log_level)

    database.set_db('proxy_db', config.proxy_postgres_conn_url,
                    engine_options={
                        'pool_size': config.pool_size,
                        'max_overflow': config.pool_max_overflow,
                        'pool_pre_ping': config.pool_pre_ping,
                        'pool_recycle': config.pool_recycle,
                        'pool_timeout': config.pool_timeout,
                    })

    constants.set_consts(ingest_app_feed_uri=config.ingest_app_feed_uri)

    register_routers(app)
    register_exceptions_handlers(app)
    register_middlewares(app)
    return app
