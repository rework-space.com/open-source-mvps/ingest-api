from fastapi import FastAPI

import exceptions
from config import AdminAppSettings
from dependencies import database
from logger import setup_logging
from middlewares import logging_middleware, CorrelationIdMiddleware
from routers import root, healtz, v1_admin
from utils.openapi import load_json_openapi


def register_routers(app: FastAPI):
    healtz.register_healtz_routers(app)
    root.register_root_routers(app)
    v1_admin.register_v1_inner_routers(app)


def register_exceptions_handlers(app: FastAPI):
    exceptions.register_exceptions_handlers(app)


def register_middlewares(app: FastAPI):
    app.middleware('http')(logging_middleware)
    app.add_middleware(CorrelationIdMiddleware)


def create_admin_app(config: AdminAppSettings | None = None):
    if config is None:
        config = AdminAppSettings()

    app = FastAPI(redoc_url=None,
                  title='Ingest API Admin')
    setup_logging(log_level=config.log_level)

    app.openapi_schema = load_json_openapi(config.paths.inner_openapi_spec)

    database.set_db('admin_db', config.admin_postgres_conn_url,
                    engine_options={
                        'pool_size': config.pool_size,
                        'max_overflow': config.pool_max_overflow,
                        'pool_pre_ping': config.pool_pre_ping,
                        'pool_recycle': config.pool_recycle,
                        'pool_timeout': config.pool_timeout,
                    })

    register_routers(app)
    register_exceptions_handlers(app)
    register_middlewares(app)
    return app
