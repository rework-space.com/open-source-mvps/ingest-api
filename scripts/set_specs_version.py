import json
import sys
from pathlib import Path


if len(sys.argv) <= 1:
    raise ValueError('A new version not specified')


root = Path(__file__).parent.parent.absolute()
openapi_ingest_spec = root / 'docs' / 'ingest-openapi.json'
openapi_inner_spec = root / 'docs' / 'admin-openapi.json'

new_version = sys.argv[1]

# ingest spec
ingest_spec = json.load(openapi_ingest_spec.open())
ingest_spec['info']['version'] = new_version
json.dump(ingest_spec, openapi_ingest_spec.open('w'))

# inner spec
ingest_spec = json.load(openapi_inner_spec.open())
ingest_spec['info']['version'] = new_version
json.dump(ingest_spec, openapi_inner_spec.open('w'))
