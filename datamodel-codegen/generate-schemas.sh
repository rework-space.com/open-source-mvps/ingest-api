datamodel-codegen --input docs/ingest-openapi.json \
                  --input-file-type=openapi \
                  --use-title \
                  --output ingest_api/schemas.py \
                  --target-python-version=3.11 \
                  --openapi-scopes=schemas \
                  --openapi-scopes=paths \
