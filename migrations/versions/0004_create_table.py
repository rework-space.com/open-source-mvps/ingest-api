"""create_table

Revision ID: 0004
Revises: 0003
Create Date: 2023-07-28 17:06:44.436975

"""
from alembic import op
import sqlalchemy as sa
from ingest_api import models

# revision identifiers, used by Alembic.
revision = '0004'
down_revision = '0003'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('admin_users',
    sa.Column('id', sa.UUID(), server_default=sa.text('uuid_generate_v4()'), nullable=False),
    sa.Column('name', sa.TEXT(), nullable=True),
    sa.Column('enable', sa.Boolean(), server_default='f', nullable=False),
    sa.Column('api_key', models.ingest_api.domains.api_key.ApiKey(), server_default=sa.text('ingest_api.generate_api_key()'), nullable=False),
    sa.Column('created_at', sa.DateTime(timezone=True), server_default=sa.text('now()'), nullable=False),
    sa.PrimaryKeyConstraint('id', name=op.f('pk__admin_users')),
    sa.UniqueConstraint('api_key', name=op.f('uq__admin_users__api_key')),
    schema='ingest_api'
    )
    op.create_table('aliases_configs',
    sa.Column('_id', sa.UUID(), server_default=sa.text('uuid_generate_v4()'), nullable=False),
    sa.Column('country', sa.TEXT(), server_default='country', nullable=False),
    sa.Column('email', sa.TEXT(), server_default='email', nullable=False),
    sa.Column('email_hash_md5_content', sa.TEXT(), server_default='email_hash_md5_content', nullable=False),
    sa.Column('firstname', sa.TEXT(), server_default='firstname', nullable=False),
    sa.Column('lastname', sa.TEXT(), server_default='lastname', nullable=False),
    sa.Column('ip', sa.TEXT(), server_default='ip', nullable=False),
    sa.Column('url', sa.TEXT(), server_default='url', nullable=False),
    sa.Column('date', sa.TEXT(), server_default='date', nullable=False),
    sa.Column('dob', sa.TEXT(), server_default='dob', nullable=False),
    sa.Column('gender', sa.TEXT(), server_default='gender', nullable=False),
    sa.Column('address1', sa.TEXT(), server_default='address1', nullable=False),
    sa.Column('address2', sa.TEXT(), server_default='address2', nullable=False),
    sa.Column('zip', sa.TEXT(), server_default='zip', nullable=False),
    sa.Column('city', sa.TEXT(), server_default='city', nullable=False),
    sa.Column('state', sa.TEXT(), server_default='state', nullable=False),
    sa.Column('vertical', sa.TEXT(), server_default='vertical', nullable=False),
    sa.Column('language', sa.TEXT(), server_default='language', nullable=False),
    sa.Column('phone_number1', sa.TEXT(), server_default='phone_number1', nullable=False),
    sa.Column('phone_number2', sa.TEXT(), server_default='phone_number2', nullable=False),
    sa.Column('age', sa.TEXT(), server_default='age', nullable=False),
    sa.Column('_created_at', sa.DateTime(timezone=True), server_default=sa.text('now()'), nullable=False),
    sa.PrimaryKeyConstraint('_id', name=op.f('pk__aliases_configs')),
    schema='ingest_api'
    )
    op.create_table('public_endpoint_configs',
    sa.Column('id', sa.UUID(), server_default=sa.text('uuid_generate_v4()'), nullable=False),
    sa.Column('enable', sa.Boolean(), server_default='f', nullable=False),
    sa.Column('path_parameter', sa.String(), nullable=False),
    sa.Column('created_at', sa.DateTime(timezone=True), server_default=sa.text('now()'), nullable=False),
    sa.PrimaryKeyConstraint('id', name=op.f('pk__public_endpoint_configs')),
    sa.UniqueConstraint('path_parameter', name=op.f('uq__public_endpoint_configs__path_parameter')),
    schema='ingest_api'
    )
    op.create_table('proxy_configs',
    sa.Column('id', sa.UUID(), server_default=sa.text('uuid_generate_v4()'), nullable=False),
    sa.Column('name', sa.String(), nullable=True),
    sa.Column('aliases_config_id', sa.UUID(), nullable=False),
    sa.Column('public_endpoint_config_id', sa.UUID(), nullable=True),
    sa.Column('created_at', sa.DateTime(timezone=True), server_default=sa.text('now()'), nullable=False),
    sa.ForeignKeyConstraint(['aliases_config_id'], ['ingest_api.aliases_configs._id'], name=op.f('fk__proxy_configs__aliases_config_id__aliases_configs'), onupdate='RESTRICT', ondelete='RESTRICT'),
    sa.ForeignKeyConstraint(['public_endpoint_config_id'], ['ingest_api.public_endpoint_configs.id'], name=op.f('fk__proxy_configs__public_endpoint_config_id__public_endpoint_configs'), onupdate='RESTRICT', ondelete='RESTRICT'),
    sa.PrimaryKeyConstraint('id', name=op.f('pk__proxy_configs')),
    sa.UniqueConstraint('public_endpoint_config_id', name=op.f('uq__proxy_configs__public_endpoint_config_id')),
    schema='ingest_api'
    )
    op.create_table('users_configs',
    sa.Column('id', sa.UUID(), server_default=sa.text('uuid_generate_v4()'), nullable=False),
    sa.Column('proxy_config_id', sa.UUID(), nullable=True),
    sa.Column('created_at', sa.DateTime(timezone=True), server_default=sa.text('now()'), nullable=False),
    sa.ForeignKeyConstraint(['proxy_config_id'], ['ingest_api.proxy_configs.id'], name=op.f('fk__users_configs__proxy_config_id__proxy_configs'), onupdate='RESTRICT', ondelete='RESTRICT'),
    sa.PrimaryKeyConstraint('id', name=op.f('pk__users_configs')),
    schema='ingest_api'
    )
    op.create_table('users',
    sa.Column('id', sa.UUID(), server_default=sa.text('uuid_generate_v4()'), nullable=False),
    sa.Column('name', sa.TEXT(), nullable=True),
    sa.Column('data_asset_id', sa.UUID(), nullable=False),
    sa.Column('user_config_id', sa.UUID(), nullable=False),
    sa.Column('enable', sa.Boolean(), server_default='f', nullable=False),
    sa.Column('api_key', models.ingest_api.domains.api_key.ApiKey(), server_default=sa.text('ingest_api.generate_api_key()'), nullable=False),
    sa.Column('created_at', sa.DateTime(timezone=True), server_default=sa.text('now()'), nullable=False),
    sa.ForeignKeyConstraint(['user_config_id'], ['ingest_api.users_configs.id'], name=op.f('fk__users__user_config_id__users_configs'), onupdate='RESTRICT', ondelete='RESTRICT'),
    sa.PrimaryKeyConstraint('id', name=op.f('pk__users')),
    sa.UniqueConstraint('api_key', name=op.f('uq__users__api_key')),
    schema='ingest_api'
    )
    op.create_table('data',
    sa.Column('_id', sa.UUID(), server_default=sa.text('uuid_generate_v4()'), nullable=False),
    sa.Column('country', sa.TEXT(), nullable=True),
    sa.Column('email', sa.TEXT(), nullable=False),
    sa.Column('email_hash_md5_content', sa.TEXT(), nullable=True),
    sa.Column('firstname', sa.TEXT(), nullable=True),
    sa.Column('lastname', sa.TEXT(), nullable=True),
    sa.Column('ip', sa.TEXT(), nullable=True),
    sa.Column('url', sa.TEXT(), nullable=True),
    sa.Column('date', sa.TEXT(), nullable=True),
    sa.Column('dob', sa.TEXT(), nullable=True),
    sa.Column('gender', sa.TEXT(), nullable=True),
    sa.Column('address1', sa.TEXT(), nullable=True),
    sa.Column('address2', sa.TEXT(), nullable=True),
    sa.Column('zip', sa.TEXT(), nullable=True),
    sa.Column('city', sa.TEXT(), nullable=True),
    sa.Column('state', sa.TEXT(), nullable=True),
    sa.Column('vertical', sa.TEXT(), nullable=True),
    sa.Column('language', sa.TEXT(), nullable=True),
    sa.Column('phone_number1', sa.TEXT(), nullable=True),
    sa.Column('phone_number2', sa.TEXT(), nullable=True),
    sa.Column('age', sa.Integer(), nullable=True),
    sa.Column('_created_at', sa.DateTime(timezone=True), server_default=sa.text('now()'), nullable=False),
    sa.Column('_user_id', sa.UUID(), nullable=True),
    sa.ForeignKeyConstraint(['_user_id'], ['ingest_api.users.id'], name=op.f('fk__data___user_id__users'), onupdate='RESTRICT', ondelete='RESTRICT'),
    sa.PrimaryKeyConstraint('_id', name=op.f('pk__data')),
    schema='ingest_api'
    )
    op.create_table('invalid_requests',
    sa.Column('id', sa.UUID(), server_default=sa.text('uuid_generate_v4()'), nullable=False),
    sa.Column('user_id', sa.UUID(), nullable=True),
    sa.Column('request_id', sa.UUID(), nullable=False),
    sa.Column('client', sa.TEXT(), nullable=False),
    sa.Column('method', sa.TEXT(), nullable=False),
    sa.Column('path', sa.TEXT(), nullable=False),
    sa.Column('query_params', sa.TEXT(), nullable=True),
    sa.Column('body', sa.TEXT(), nullable=True),
    sa.Column('error', sa.TEXT(), nullable=True),
    sa.Column('created_at', sa.DateTime(timezone=True), server_default=sa.text('now()'), nullable=False),
    sa.ForeignKeyConstraint(['user_id'], ['ingest_api.users.id'], name=op.f('fk__invalid_requests__user_id__users'), onupdate='RESTRICT', ondelete='RESTRICT'),
    sa.PrimaryKeyConstraint('id', name=op.f('pk__invalid_requests')),
    schema='ingest_api'
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('invalid_requests', schema='ingest_api')
    op.drop_table('data', schema='ingest_api')
    op.drop_table('users', schema='ingest_api')
    op.drop_table('users_configs', schema='ingest_api')
    op.drop_table('proxy_configs', schema='ingest_api')
    op.drop_table('public_endpoint_configs', schema='ingest_api')
    op.drop_table('aliases_configs', schema='ingest_api')
    op.drop_table('admin_users', schema='ingest_api')
    # ### end Alembic commands ###
