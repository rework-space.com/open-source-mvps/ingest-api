"""init_api_key_tools

Revision ID: 0003
Revises: 0002
Create Date: 2023-03-14 08:22:58.680563

"""
from alembic import op
from alembic_utils.pg_function import PGFunction

# revision identifiers, used by Alembic.
revision = '0003'
down_revision = '0002'
branch_labels = None
depends_on = None


def upgrade() -> None:
    ingest_api_generate_api_key = PGFunction(
        schema="ingest_api",
        signature="generate_api_key()",
        definition="returns text as\n        $$\n        declare\n          chars text[] := '{0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z}';\n          result text := '';\n          i integer := 0;\n        begin\n          for i in 1..32 length loop\n            result := result || chars[1+random()*(array_length(chars, 1)-1)];\n          end loop;\n          return result;\n        end;\n        $$ language plpgsql"
    )
    op.create_entity(ingest_api_generate_api_key)
    op.execute("CREATE DOMAIN ingest_api.api_key AS VARCHAR(32) NOT NULL CHECK (value ~ '[a-zA-Z0-9]{32}');")


def downgrade() -> None:

    ingest_api_generate_api_key = PGFunction(
        schema="ingest_api",
        signature="generate_api_key()",
        definition="returns text as\n        $$\n        declare\n          chars text[] := '{0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z}';\n          result text := '';\n          i integer := 0;\n        begin\n          for i in 1..32 length loop\n            result := result || chars[1+random()*(array_length(chars, 1)-1)];\n          end loop;\n          return result;\n        end;\n        $$ language plpgsql"
    )
    op.drop_entity(ingest_api_generate_api_key)
    op.execute("DROP DOMAIN ingest_api.api_key;")
