"""init_schemas

Revision ID: 0001
Revises: 
Create Date: 2023-03-13 13:26:05.411734

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0001'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.execute('create schema if not exists ingest_api')


def downgrade() -> None:
    op.execute('drop schema if exists ingest_api')
