"""init_extension

Revision ID: 0002
Revises: 0001
Create Date: 2023-03-13 13:33:55.540260

"""
from alembic import op
import sqlalchemy as sa
from alembic_utils.pg_extension import PGExtension
from sqlalchemy import text as sql_text

# revision identifiers, used by Alembic.
revision = '0002'
down_revision = '0001'
branch_labels = None
depends_on = None


def upgrade() -> None:
    public_uuid_ossp = PGExtension(
        schema="public",
        signature="uuid-ossp"
    )
    op.create_entity(public_uuid_ossp)


def downgrade() -> None:
    public_uuid_ossp = PGExtension(
        schema="public",
        signature="uuid-ossp"
    )
    op.drop_entity(public_uuid_ossp)

